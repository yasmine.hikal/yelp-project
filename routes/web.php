 <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Frontend\HomeController@index');
Route::get('/contact_us','Frontend\ContactController@index')->name('contact_us');
Route::get('/post_contact','Frontend\ContactController@post_contact')->name('post_contact');

Route::get('/events','Frontend\EventController@index')->name('events');
Route::get('/interest_event','Frontend\EventController@interest_event')->name('interest_event');

Route::get('/event_details/{id}','Frontend\EventController@event_details')->name('event_details');

Route::get('/category_posts/{id}','Frontend\PostController@index')->name('category_posts');
Route::get('/post_details/{id}','Frontend\PostController@post_details')->name('post_details');
Route::post('/post_details/{id}/review','Frontend\PostController@review')->name('review');
Route::post('/search','Frontend\PostController@search')->name('search');

Route::get('/location_search','Frontend\PostController@get_location_search')->name('location_search');
Route::post('/post_location_search','Frontend\PostController@post_location_search')->name('post_location_search');







Auth::routes();

Route::group(['middleware' => ['auth']] , function(){

    Route::get('profile' , 'Frontend\UserController@index')->name('profile');
    Route::post('update_profile' , 'Frontend\UserController@update_profile')->name('update_profile');
});


Route::group(['middleware' => ['auth']], function () {

    Route::get('dashboard' , 'Backend\DashboardController@index')->name('dashboard');
    Route::resource('category', 'Backend\CategoryController');
    Route::resource('user' , 'Backend\UserController');
    Route::resource('event' , 'Backend\EventController');
    Route::resource('post' , 'Backend\PostController');
    Route::resource('contact' , 'Backend\ContactUsController');

    Route::get('delete_image' , 'Backend\PostController@deleteImage');

});
 

