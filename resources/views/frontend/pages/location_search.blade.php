@extends('frontend.template.app')


@section('page_title' , 'Location Search')

@section('styles')

    <style>

        #map {
            height: 700px;
            width: 100%;
        }

    </style>

@endsection
@section('content')

    <!-- Inner Banner -->
    <section id="inner-banner-2">
        <div class="container">
            <div class="row">

                <div class="col-md-12 text-center">
                    <div class="inner_banner_2_detail">
                        <h2>Listing Details</h2>
                        <p><a href="{{url('/')}}">Home</a> <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                            Location Search</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Inner Banner -->


    <section id="banner-map">
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-3 col-sm-3 col-xs-12">

                    {!! Form::open(['route'=>'post_location_search' , 'method'=>'POST']) !!}
                    <div class="profile-login-bg">
                        <h2><span><i class="fa fa-map-marker"></i></span> Geo <span>Location</span></h2>

                        <div class="row p_b30">
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="name">Latitude</label>
                                    <input class="form-control"  required="required" id="lat" placeholder="drag the marker" name="lat"  type="text">
                                </div>
                                <!--/.form-group-->
                            </div>
                            <!--/.col-md-3-->
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="email">Longitude</label>
                                    <input class="form-control"  required="required" id="long" placeholder="drag the marker" name="long"  type="text">
                                </div>
                                <!--/.form-group-->
                            </div>

                        </div>

                        <h2><span><i class="fa fa-info-circle"></i></span> Select Category</h2>

                        <div class="form-group">
                            <label for="state">Categories</label>
                            <select class="form-control" name="category_id">
                                <option selected disabled>select category</option>
                                @foreach($categories as $category)
                                    <option value="{!! $category->id !!}">{!! $category->name !!}</option>
                                @endforeach
                            </select>

                        </div>


                            <button type="submit" class="btn btn-small btn-default btn-block" id="submit">Find near by </button>

                    </div>
                    {!! Form::close() !!}

                </div>

                <div class="col-md-9 col-sm-9 col-xs-12">

                    {{--<div id="finddo-wrapper">--}}
                        {{--<!-- Places panel (auto removable) -->--}}
                        {{--<div id="finddo-places" class="hidden-xs">--}}
                            {{--<div id="finddo-results-num"></div>--}}
                        {{--</div>--}}
                        {{--<!-- Map wrapper -->--}}
                        {{--<div id="finddo-canvas"></div>--}}
                    {{--</div>--}}

                    <div id="map"></div>

            </div>
            </div>
        </div>
    </section>


@endsection


@section('scripts')

    {!! Html::script('frontend/js/map/jquery-finddo.js') !!}

    {!! Html::script('frontend/js/map/markercluster.min.js') !!}

    {!! Html::script('frontend/js/map/custom-map.js') !!}
    <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&amp;libraries=places&amp;?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U"></script>

    {!! Html::script('frontend/js//modernizr.custom.26633.js') !!}

    {!! Html::script('frontend/js/jquery.gridrotator.js') !!}

    {{--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>--}}

    <script>



        window.onload = function() {

            var latlng = new google.maps.LatLng(27.276551611306182 , 30.703368206249934);
            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 6,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: 'Drag this marker on you current location to begin fetch things near by you',
                draggable: true
            });
            google.maps.event.addListener(marker, 'dragend', function(a) {


                $('#lat').val(a.latLng.lat()) ;
                $('#long').val(a.latLng.lng()) ;



            });
        };




    </script>


@endsection

