@extends('frontend.template.app')


@section('page_title' , 'Home page')


@section('content')


    <!-- Inner Banner -->
    <section id="inner-banner-2">
        <div class="container">
            <div class="row">

                <div class="col-md-12 text-center">
                    <div class="inner_banner_2_detail">
                        <h2>Events</h2>
                        <p><a href="{{url('/')}}">Home</a> <i class="fa fa-angle-double-right" aria-hidden="true"></i> Events</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Popular Listing -->
    <section id="popular-listing" class="p_b70 p_t70">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">


                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="profile">
                            <div class="row">

                                @if($events)

                                    @foreach($events as $event)
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="popular-listing-box">
                                        <div class="popular-listing-img">
                                            <figure class="effect-ming"> <img style="height: 189px;" src="{{Request::root()}}/uploads/events_images/{{$event->image}}" alt="image">
                                                <figcaption>

                                                </figcaption>
                                            </figure>
                                        </div>
                                        <div class="popular-listing-detail">
                                            <h3><a href="{{route('event_details',$event->id)}}">{{$event->name}}</a></h3>
                                            <p>{{strip_tags($event->description)}}.</p>
                                        </div>
                                        <div class="popular-listing-add"> <span><i class="fa fa-map-marker" aria-hidden="true"></i> New York City</span> <span></span> </div>
                                    </div>
                                </div>

                                    @endforeach
                            </div>
                            @else


                                @endif
                        </div>


                    </div>

                </div>

            </div>
        </div>
    </section>
    <!-- Popular Listing -->





@endsection

