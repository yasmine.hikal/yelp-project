@extends('frontend.template.app')


@section('page_title' , 'Home page')


@section('content')


    <!-- Inner Banner -->
    <section id="inner-banner-2">
        <div class="container">
            <div class="row">

                <div class="col-md-12 text-center">
                    <div class="inner_banner_2_detail">
                        <h2>Contact Us</h2>
                        <p><a href="{{url('/')}}">Home</a> <i class="fa fa-angle-double-right" aria-hidden="true"></i> Contact Us</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Inner Banner -->

    <!-- Contact Us -->
    <section id="contact-us" class="p_b70 p_t70">

        <div class="container">

            <div class="row">

                <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">

                    <div class="contact-bg">

                        <div class="heading">
                            <h2>Get in Touch <span>With Us</span></h2>
                        </div>
{!! Form::open(['route'=>['post_contact'] ,'class'=>"contact-form", 'id'=>'contact_form', 'method' =>'POST' ]) !!}

                            <div class="row">

                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Your Name">
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <input type="email" name="email"  id="email" class="form-control" placeholder="Your E-mail">
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone">
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="message" id="message" class="form-control" placeholder="Your message"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">



                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit">Submit</button>
                                    </div>
                                </div>

                            </div>



                        {!! Form::close() !!}
                   <div id="contact_msg"></div>

                    </div>

                    {{--<div class="row">--}}

                        {{--<div class="col-md-3 col-sm-3 col-xs-12 text-center">--}}
                            {{--<div class="address-box">--}}
                                {{--<p><i class="fa fa-phone" aria-hidden="true"></i>--}}
                                {{--</p>--}}
                                {{--<h4>Mobile/Phone</h4>--}}
                                {{--<p>+(10) 123 456 7896</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="col-md-3 col-sm-3 col-xs-12 text-center">--}}
                            {{--<div class="address-box">--}}
                                {{--<p><i class="fa fa-envelope" aria-hidden="true"></i>--}}
                                {{--</p>--}}
                                {{--<h4>E-Mail</h4>--}}
                                {{--<p><a href="#">support@find_do.com</a>--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="col-md-3 col-sm-3 col-xs-12 text-center">--}}
                            {{--<div class="address-box">--}}
                                {{--<p><i class="fa fa-globe" aria-hidden="true"></i>--}}
                                {{--</p>--}}
                                {{--<h4>Website</h4>--}}
                                {{--<p><a href="#">www.finddodirectory.com</a>--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="col-md-3 col-sm-3 col-xs-12 text-center">--}}
                            {{--<div class="address-box">--}}
                                {{--<p><i class="fa fa-map-marker" aria-hidden="true"></i>--}}
                                {{--</p>--}}
                                {{--<h4>Address</h4>--}}
                                {{--<p>Spy hosting, 06 Highley St,--}}
                                    {{--<br>Victoria, Australia.</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}

                </div>

            </div>

        </div>

    </section>
    <!-- Contact Us -->





@endsection


@section('scripts')





    <script type="text/javascript">

        $('#contact_form').on('submit', function(e){
            e.preventDefault();


            var formData = {
                name : $('#name').val() ,
                email : $('#email').val() ,
                phone : $('#phone').val() ,
                message : $('#message').val() ,


            };

            $.ajax({
                url: $(this).attr('action'),
                type: 'get',
                data: formData,
                dataType:'json',
                success: function (data){


                    if(data.status == 'contact_sent'){
                        jQuery('#contact_msg').html("<p class='alert alert-success text-center'>{{trans('Message sent successfully')}}<p>");


                    }
                    if(data.status == 'error'){
                        jQuery('#contact_msg').html("<p class='alert alert-danger text-center'>{{trans('Error sending message')}}<p>");
                    }



                }


            });


        });

    </script>
    @endsection
