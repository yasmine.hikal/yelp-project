@extends('frontend.template.app')


@section('page_title' , 'Home page')

@section('styles')

    <style>
        #map {
            height: 400px;
            width: 100%;
        }
        .hidden{
            display: none ;
        }
    </style>


@endsection

@section('content')


    <!-- Inner Banner -->
    <section id="inner-banner-2">
        <div class="container">
            <div class="row">

                <div class="col-md-12 text-center">
                    <div class="inner_banner_2_detail">
                        <h2>Profile</h2>
                        <p><a href="{{url('/')}}">Home</a> <i class="fa fa-angle-double-right" aria-hidden="true"></i> Profile</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Inner Banner -->

    <!-- Profile -->
    <section id="profile" class="p_b70 p_t70 bg_lightgry">

        <div class="container">
            <div class="row">

                <div class="col-md-3 col-sm-3 col-xs-12">

                    <div class="profile-leftbar">
                        <div id="profile-picture" class="profile-picture dropzone dz-clickable">
                            <input name="file" type="file">
                            {{--<div class="dz-default dz-message">--}}
                            {{--</div>--}}
                            @if($user->image == "")
                            <img src="{{Request::root()}}/user_main_image.png" alt="">
                                @else

                                <img src="{{Request::root()}}/uploads/users_images/{{$user->image}}" alt="">
                            @endif
                        </div>
                    </div>

                    <div class="profile-list">
                        <ul>
                            <li class="active">
                                <a href="#"><i class="fa fa-user-o" aria-hidden="true"></i> My Profile</a>
                            </li>
                            {{--<li>--}}
                                {{--<a href="#"><i class="fa fa-sliders" aria-hidden="true"></i> Reviews</a>--}}
                            {{--</li>--}}

                            {{--<li>--}}
                                {{--<a href="#"><i class="fa fa-cogs" aria-hidden="true"></i> Events</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#"><i class="fa fa-lock" aria-hidden="true"></i> Log Out</a>--}}
                            {{--</li>--}}
                        </ul>
                    </div>

                </div>

                <div class="col-md-9 col-sm-9 col-xs-12">


                    <div class="profile-login-bg">
                        <h2><span><i class="fa fa-user"></i></span> Personal <span>Info</span></h2>
            {!! Form::open(['route'=>['update_profile'] ,  'method' =>'POST' ,'files'=>true]) !!}

                        <div class="row p_b30">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input class="form-control" value="{{$user->name}}" id="name" name="name"  type="text">
                                </div>
                                <!--/.form-group-->
                            </div>
                            <!--/.col-md-3-->
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input class="form-control" id="email" name="email" value="{{$user->email}}" type="email">
                                </div>
                                <!--/.form-group-->
                            </div>

                            <!--/.col-md-3-->
                            <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <label for="image">User Image</label>
                                <input type="file" name="image" class="dropify">

                            </div>
                            </div>
                            <!--/.col-md-3-->
                        </div>

                        <h2><span><i class="fa fa-map-marker"></i></span> Address</h2>


                        <!--/.form-group-->
                        <div class="form-group">
                        <div class="card-body col-md-12">

                            <div id="map"></div>
                            <input type="hidden" placeholder="{{ trans('backend.latitude') }}"  value="27.276551611306182"  name="latitude"  class="form-control form-control-line" id="latiude">
                            <input type="hidden" placeholder="{{ trans('backend.longitude') }}" value="30.703368206249934" name="longitude"  class="form-control form-control-line" id="longitude">

                        </div>
                        </div>
                    </br>
                        <!--/.form-group-->

                        <div class="form-group">
                            <button type="submit" class="btn btn-large btn-default" id="submit">Save Changes</button>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </section>
    <!-- Popular Listing -->



@endsection

@section('scripts')


    <script>
        // for image input
        jQuery(document).ready(function() {

            $('.dropify').dropify();



        });



    </script>


    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

    <script>



        window.onload = function() {

            var latlng = new google.maps.LatLng(27.276551611306182 , 30.703368206249934);
            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 5,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: 'Set lat/lon values for this post',
                draggable: true
            });
            google.maps.event.addListener(marker, 'dragend', function(a) {
                console.log(a);

                $('#latiude').val(a.latLng.lat()) ;
                $('#longitude').val(a.latLng.lng()) ;

            });
        };




    </script>



    @endsection