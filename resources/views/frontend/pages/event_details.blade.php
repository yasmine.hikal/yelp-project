@extends('frontend.template.app')


@section('page_title' , 'Home page')


@section('content')

    <style>


        #map {
            height: 150px;
            width: 100%;
        }
    </style>

    <!-- Inner Banner -->
    <section id="inner-banner-2">
        <div class="container">
            <div class="row">

                <div class="col-md-12 text-center">
                    <div class="inner_banner_2_detail">
                        <h2>Events</h2>
                        <p><a href="{{url('/')}}">Home</a> <i class="fa fa-angle-double-right" aria-hidden="true"></i> Events</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Popular Listing -->
    <section id="blog" class="p_b70 p_t70 bg_lightgry">
        <div class="container">

            <div class="row">

                <div class="col-md-9 col-sm-9 col-xs-12">

                    <div class="blog heading">

                        <h2>{{$event->name}}</h2>

                        <div class="blog-img">
                            <img style="height: 300px;" src="{{asset("uploads/events_images/$event->image")}}" alt="image">
                        </div>

                        <div class="blog-detail">
                            <ul class="blog-admin">

                                <li><i class="fa fa-clock-o"></i>
                                    <a href="#">
                            FROM: <span style="color: #1fb7a6;">{{$event->from}}</span>

                            TO: <span style="color: #1fb7a6;">{{$event->to}}</span>
                                    </a>
                                </li>


                                @if(Auth::user() )
                                <li>

                                    <label class="form-check-label">
                                                    <input  type="radio" class="form-check-input optionsRadios" name="optionsRadios" id="optionsRadios1" value="1" @if($event_user) {{  $event_user->interested == '1'?'checked':''}} @endif >
                                                   <span style="color: #1fb7a6;"> Interested</span>
                                                    <i class="input-helper"></i></label>


                                                <label class="form-check-label">
                                                    <input  type="radio" class="form-check-input optionsRadios" name="optionsRadios" id="optionsRadios2" value="0" @if($event_user) {{$event_user->interested != '1'?'checked':''}} @endif>
                                                    <span style="color: #1fb7a6;"> Not Interested </span>                                                  <i class="input-helper"></i></label>



                                </li>
                                @endif
                            </ul>
                            <p>{{strip_tags($event->description)}}. </p>
                        </div>

                    </div>




                </div>

                <div class="col-md-3 col-sm-3 col-xs-12">



                    <div class="right-bar bg_white">
                        <h4>Get <span>Directions</span></h4>
                        <div id="map"></div>
                        <div id="cd-google-map">
                            {{--<div id="google-container"></div>--}}
                            {{--<div id="cd-zoom-in"></div>--}}
                            {{--<div id="cd-zoom-out"></div>--}}

                            {{--<p><i class="fa fa-map-marker" aria-hidden="true"></i> 57 East 57th Street, New York 10022--}}
                            {{--</p>--}}

                            </p>
                        </div>
                    </div>


                </div>

            </div>

        </div>
    </section>





@endsection

@section('scripts')

    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

    <script>



        window.onload = function() {

            var latlng = new google.maps.LatLng('{!! $event->latitude !!}' , '{!! $event->longitude !!}');
            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: 'Set lat/lon values for this post',
                draggable: false
            });
            google.maps.event.addListener(marker, 'dragend', function(a) {
                console.log(a);



            });
        };




    </script>



    <script type="text/javascript">

        $('.optionsRadios').on('click', function(e){


            var formData = {
                option : $('.optionsRadios:checked').val() ,
                event_id :'{!! $event->id !!}'  ,


            };

           // alert('{!! $event->id !!}')

            $.ajax({
                url: '{!! route('interest_event') !!}',
                type: 'get',
                data: formData,
                dataType:'json',
                success: function (data){



                }


            });


        });

    </script>



@endsection
