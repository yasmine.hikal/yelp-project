@extends('frontend.template.app')


@section('page_title' , 'Home page')


@section('content')



    <!-- Banner -->
    <section id="banner-2" class="animated-bg banner-2-bg">

        <div class="container">
            <div class="inner-text-3">

                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="banner-text-3">
                            <h2>well come in <span>FIND DO </span>directory</h2>
                            <h3>Find The Best Place To be there</h3>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="banner-search">
                            {!! Form::open(['route'=>['search'] ,  'method' =>'POST' ,'files'=>true]) !!}

                            <div class="advance-search">

                                <div class="dir-search">
                                    <div style="width: 525px" class="single-query form-group  ">
                                        <input type="text" name="key" class="keyword-input" placeholder="Enter keyword..." required>
                                    </div>

                                    <div style="width: 300px" class="single-query form-group">
                                        <div class="intro">
                                            <select name="category_id">
                                                @if($categories)
                                                    @foreach($categories as $category)
                                                <option class="active" value="{{$category->id}}">{{$category->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="search-btn">
                                    <button type="submit">Search <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12 text-center p_t70">
                        <div class="banner-text-3">
                            <h3>What Do You Need Today?</h3>
                            <p>Expolore top-rated attractions, activities and more</p>
                            <img src="{{Request::root()}}/frontend/images/banner-arrow.png" alt="image">
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </section>
    <!-- Banner -->

    <!-- Directory Category -->
    <section id="directory-category-3" class="p_b70">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                  @if($categories)
                    @foreach($categories as $category)
                    <div class="directory-category-box3">
                        <span><i class="{{$category->icon}}" aria-hidden="true"></i></span>
                        <a href="{{route('category_posts',$category->id)}}"><h3>{{$category->name}}</h3></a>
                        <p>
                            @if(count($category->postcount)>0)
                            @foreach($category->postcount as $count)
                        {{$count->postcount}}
                               @endforeach
                                @else
                            0
                            @endif
                        </p>
                        <div class="directory-category-overlay"></div>
                    </div>
                    @endforeach
                  @endif

                </div>
            </div>
        </div>
    </section>
    <!-- Directory Category -->

    <!-- Most visited places -->
    <section id="post-visited-places">

        <div class="container over-hide">

            <div class="row">
                <div class="col-md-12 heading text-center">
                    <h2><span>Places</span> </h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt utet dolore magna aliqua. Ut enim ad minim veniam</p>
                </div>
            </div>

            <div class="row">
                <div id="places-slider" class="owl-carousel owl-theme">

                    @foreach($posts as $post)
                    <div class="item">
                        <div class="popular-listing-box">
                            <div class="popular-listing-img">
                                <figure class="effect-ming"> <img style="height: 202px" src="{{Request::root()}}/uploads/posts/{{$post->main_image}}" alt="image">
                                    <figcaption>
                                        <ul>
                                            <li><a  href="{{route('post_details',$post->id)}}"><i class="fa fa-sign-in" aria-hidden="true"></i></a>
                                            </li>
                                        </ul>
                                    </figcaption>
                                </figure>
                            </div>

                            <div class="popular-listing-detail">
                                <h3><a href="{{route('post_details',$post->id)}}">{{$post->name}}</a></h3>
                                <span>Category: <a href="#">{{$post->category->name}}</a></span>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> 57 East 57th Street, New York 10022</p>
                            </div>

                            <ul class="place-listing-add">
                                <li>

                                    @if(count($post->post_rates_count)>0)

                                        <a href="{{route('post_details',$post->id)}}"><em>See all reviews </em></a>
                                        @foreach($post->post_rates_count as $count)

                                            ( {{ $count->post_rates_count }} )
                                        @endforeach
                                    @else
                                        <em>no reviews yet !</em>
                                    @endif

                                    </li>
                                <li>
                                    {{--<div class="my-rating_{!! $count->id !!}"></div>--}}
                                    {{--<img src="{{Request::root()}}/frontend/images/stars.png" alt="image">--}}
                                </li>

                            </ul>

                        </div>
                    </div>
                    @endforeach


                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center discover">
                    <h2>Discover top-rated local businesses</h2>

                </div>
            </div>

        </div>

    </section>
    <!-- Most visited places -->

    <!-- Counter Section -->
    <div id="counter-section">
        <div class="container">

            <div class="row number-counters text-center">

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <!-- first count item -->
                    <div class="counters-item">
                        <i class="fa fa-list" aria-hidden="true"></i>
                        <!-- Set Your Number here. i,e. data-to="56" -->
                        @php
                            $places_count=\App\Post::all()->count();
                        @endphp
                        <strong data-to="{{$places_count}}">0</strong>
                        <p>Places</p>
                        <div class="border-inner"></div>
                    </div>

                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <!-- first count item -->
                    <div class="counters-item">
                        <i class="fa fa-users" aria-hidden="true"></i>
                        <!-- Set Your Number here. i,e. data-to="56" -->
                        @php
                        $users_count=\App\User::where('type','user')->get()->count();
                        @endphp
                        <strong data-to="{{$users_count}}">{{$users_count}}</strong>
                        <p>User</p>
                        <div class="border-inner"></div>
                    </div>

                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <!-- first count item -->
                    <div class="counters-item">
                        <i class="fa fa-th" aria-hidden="true"></i>
                        <!-- Set Your Number here. i,e. data-to="56" -->
                        @php
                            $cat_count=\App\Category::all()->count();
                        @endphp
                        <strong data-to="{{$cat_count}}">0</strong>
                        <p>Categories</p>
                        <div class="border-inner"></div>
                    </div>

                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <!-- first count item -->
                    <div class="counters-item">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <!-- Set Your Number here. i,e. data-to="56" -->
                        @php
                            $rates_count=\App\Rate::all()->count();
                        @endphp
                        <strong data-to="{{$rates_count}}">0</strong>
                        <p>Reviews</p>
                        <div class="border-inner"></div>
                    </div>

                </div>

            </div>

        </div>
    </div>
    <!-- Counter Section -->

    <!-- Call to Action -->
    <div id="call-to-action2">

        <div class="container">

            <div class="row">

                <div class="col-md-10 col-sm-9 col-xs-12 call-to-action-text">

                    <p>Find all things you need on FIND DO Directory, We give you a simple way.</p>

                </div>

                <div class="col-md-2 col-sm-3 col-xs-12 call-to-action-bttn">


                </div>

            </div>

        </div>

    </div>
    <!-- Cal to Action-->

    <!-- How to find -->
    <div id="how-to-find" class="p_b70 p_t70">

        <div class="container">

            <div class="row">
                <div class="col-md-12 text-center heading">
                    <h2>How it <span>Works</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt utet dolore magna aliqua. Ut enim ad minim veniam</p>
                </div>
            </div>

            <div class="row">

                <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                    <div class="how-to-find-box">
                        <img src="{{Request::root()}}/frontend/images/how-to-find1.png" alt="image">
                        <h3>Find Interesting Place</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempus mollis dui vitae finibus. Ut volutpat condimentum metus, ac pulvinar tellus.</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                    <div class="how-to-find-box">
                        <img src="{{Request::root()}}/frontend/images/how-to-find2.png" alt="image">
                        <h3>Contact a Few Owners</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempus mollis dui vitae finibus. Ut volutpat condimentum metus, ac pulvinar tellus.</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                    <div class="how-to-find-box">
                        <img src="{{Request::root()}}/frontend/images/how-to-find3.png" alt="image">
                        <h3>Explore your Day</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempus mollis dui vitae finibus. Ut volutpat condimentum metus, ac pulvinar tellus.</p>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <!-- How to find -->

    <!-- Best Things -->
    <section id="best-things">
        <div class="container">

            <div class="row">
                <div class="col-md-12 heading">
                    <h2>Explore <span>Events</span></h2>
                </div>
            </div>

            <div class="row">

                @if(count($events))
                    @foreach($events as $event)

                        <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="best-things-details">
                        <figure class="effect-ming"> <img style=" height: 285px;" src="{!! asset("uploads/events_images/$event->image") !!}" alt="image">
                            <figcaption>
                                <ul>
                                    <li>
                                        <a  href="{{route('event_details',$event->id)}}"><i class="fa fa-sign-in" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </figcaption>
                        </figure>

                        <div class="best-things-rating">
                            <div class="best-things-address">
                                <h3><span><i class="fa fa-map-marker" aria-hidden="true"></i></span>{!! $event->name !!}</h3>

                                <ul class="best-things-listing">
                                    <li><p>ewerewrwer</p>
                                    </li>

                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
                    @endforeach


                @endif


            </div>



        </div>
    </section>
    <!-- Best Things -->

    <!-- Latest News -->
    <section id="our-blog" class="p_t70 p_b70">
        <div class="container">

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="row">
                        <div class="col-md-12 heading">
                            <h2>Latest <span>Reviews</span></h2>
                        </div>
                    </div>

                    <div id="latest_news-slider" class="owl-carousel owl-theme">

                        @foreach($rates as $rate)
                        <div class="item">
                            <div class="latest_box">
                                <h3>{{$rate->post->name}}</h3>
                                <p>{{$rate->review}}</p>
                                <div class="lates_border m-b-25"></div>
                                <img style="height: 28px ; border-radius:10px" src="{{Request::root()}}/uploads/users_images/{{$rate->user->image}}" alt="image">
                                <span>by {{$rate->user->name}}</span>
                                <span><i class="fa fa-calculator" aria-hidden="true"></i>{{\Carbon\Carbon::parse($rate->created_at)->diffForHumans()}}</span>
                            </div>
                        </div>

                            @endforeach

                    </div>

                </div>


            </div>

        </div>
    </section>
    <!-- Latest News -->

    <!-- User -->
    <section id="our-user" class="p_t70 p_b70">
        <div class="container">

            <div class="row">

                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 heading">
                            <h2>Join <span>{{$users_count}}</span> User</h2>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 user-btn">
                    <a href="{{url('/login')}}">Join Now</a>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <div id="ri-grid" class="ri-grid ri-grid-size-1 ">
                        <ul>

                            @foreach($users as $user)


                                    <li>
                                        @if($user->image=="")
                                            <a href="#"><img src="{{Request::root()}}/user_main_image.png" alt="image"/>

                                            </a>
                                            @else
                                        <a href="#"><img src="{{Request::root()}}/uploads/users_images/{{$user->image}}" alt="image"/>

                                        </a>
                                            @endif
                                    </li>



                            @endforeach


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Useer -->








@endsection


@section('scripts')

    {!! Html::script('frontend/star_rating/jquery.star-rating-svg.js') !!}

    @if(count($post->post_rates_count))


        @foreach($post->post_rates_count as $count)
            <script>
                $(".my-rating_{!! $count->id !!}").starRating({
                    starSize: 25,
                    callback: function(currentRating, $el){
                        // make a server call here
                    }
                });


            </script>
            <script>
                $('.my-rating_{!! $count->id !!}').starRating('setRating', "{!! $count->star_count !!}")
                $('.my-rating_{!! $count->id !!}').starRating('setReadOnly', true);
            </script>
        @endforeach
    @endif

@endsection

