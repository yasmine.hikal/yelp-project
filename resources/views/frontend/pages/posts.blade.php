@extends('frontend.template.app')


@section('page_title' , 'Home page')


@section('content')


    <!-- Inner Banner -->
    <section id="inner-banner-2">
        <div class="container">
            <div class="row">

                <div class="col-md-12 text-center">
                    <div class="inner_banner_2_detail">
                        <h2>Places</h2>
                        <p><a href="{{url('/')}}">Home</a> <i class="fa fa-angle-double-right" aria-hidden="true"></i> {{$category}}</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Popular Listing -->
    {{--<section id="popular-listing" class="p_b70 p_t70">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}

                {{--<div class="col-md-12 col-sm-12 col-xs-12">--}}


                    {{--<!-- Tab panes -->--}}
                    {{--<div class="tab-content">--}}

                        {{--<div role="tabpanel" class="tab-pane active" id="profile">--}}
                            {{--<div class="row">--}}

                                {{--@if($posts)--}}

                                    {{--@foreach($posts as $post)--}}
                                {{--<div class="col-md-4 col-sm-6 col-xs-12">--}}
                                    {{--<div class="popular-listing-box">--}}
                                        {{--<div class="popular-listing-img">--}}
                                            {{--<figure class="effect-ming"> <img style="height: 189px;" src="{{Request::root()}}/uploads/posts/{{$post->main_image}}" alt="image">--}}
                                                {{--<figcaption>--}}

                                                {{--</figcaption>--}}
                                            {{--</figure>--}}
                                        {{--</div>--}}
                                        {{--<div class="popular-listing-detail">--}}
                                            {{--<h3><a href="{{route('post_details',$post->id)}}">{{$post->name}}</a></h3>--}}
                                            {{--<p>{{strip_tags($post->description)}}.</p>--}}
                                        {{--</div>--}}
                                        {{--<div class="popular-listing-add"> <span><i class="fa fa-map-marker" aria-hidden="true"></i> New York City</span> <span></span> </div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                    {{--@endforeach--}}
                            {{--</div>--}}
                            {{--@else--}}


                                {{--@endif--}}
                        {{--</div>--}}


                    {{--</div>--}}

                {{--</div>--}}

            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
    <!-- Popular Listing -->


    <!-- Popular Listing -->
    <section id="popular-listing" class="p_b70 p_t70">
        <div class="container">
            @if(count($posts)>0)
            <div class="row">

                <div class="col-md-9 col-sm-9 col-xs-12">

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane fade in active" id="profile">

                        <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">


                        <!-- Tab panes -->
                        <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="profile">
                        <div class="row">

                        @if($posts)

                        @foreach($posts as $post)
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="popular-listing-box">
                        <div class="popular-listing-img">
                        <figure class="effect-ming"> <img style="height: 189px;" src="{{Request::root()}}/uploads/posts/{{$post->main_image}}" alt="image">
                        <figcaption>

                                <ul>
                                    <li><a  href="{{route('post_details',$post->id)}}"><i class="fa fa-sign-in" aria-hidden="true"></i></a>
                                    </li>
                                </ul>


                        </figcaption>
                        </figure>
                        </div>
                        <div class="popular-listing-detail">
                        <h3><a href="{{route('post_details',$post->id)}}">{{$post->name}}</a></h3>
                        <p>{{ str_limit(strip_tags($post->description),50)}}.</p>
                        </div>
                        <div class="popular-listing-add"> <span><i class="fa fa-map-marker" aria-hidden="true"></i> New York City</span> <span></span> </div>
                        </div>
                        </div>

                        @endforeach
                        </div>
                        @else


                        @endif
                        </div>


                        </div>

                        </div>

                        </div>
                        </div>

                    </div>


                </div>

                <div class="col-md-3 col-sm-3 col-xs-12 listing-rightbar">

                    <div class="right-bar">
                        <h4>Category <span>List</span></h4>
                        <ul class="right-bar-listing">

                            @if($categories)
                                @foreach($categories as $category)
                            <li><a href="{{route('category_posts',$category->id)}}"><i class=" {{$category->icon}}" aria-hidden="true"></i> {{$category->name}} <span>  @if(count($category->postcount)>0)
                                            @foreach($category->postcount as $count)
                                                {{$count->postcount}}
                                            @endforeach
                                        @else
                                            0
                                        @endif
                                        </span></a>
                            </li>

                                @endforeach
                                @endif
                        </ul>
                    </div>

                    <div class="right-bar">
                        <h4>New <span>Places</span></h4>
                        @foreach($recent_posts as $post)
                        <div class="places-list">
                            <h5><a href="{{route('post_details',$post->id)}}">{{$post->name}}</a></h5>

                            <div class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <img class="media-object" style="width: 90px; height: 70px; " src="{{Request::root()}}/uploads/posts/{{$post->main_image}}" alt="image">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading"><i class="fa fa-glass" aria-hidden="true"></i> {{$post->category->name}}</h6>
                                    <img src="{{Request::root()}}/frontend/images/stars.png">
                                </div>
                            </div>
                        </div>
                        @endforeach


                    </div>


                </div>

            </div>

                @else
                <div class="row">

                    <div class="col-md-12 text-center">
                        <div class="add-listing-bg heading">
                            <h2>No <span>Results</span></h2>
                            <p>Sorry! You there is  no list .</p>
                            <p>Go and select lists  <a href="{{url('/')}}">Visit Here</a>
                            </p>
                        </div>
                    </div>

                </div>

            @endif
        </div>
    </section>
    <!-- Popular Listing -->


@endsection

