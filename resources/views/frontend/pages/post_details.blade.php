@extends('frontend.template.app')


@section('page_title' , 'Home page')

@section('styles')

    {!! Html::style('backend/vendors/css/vendor.bundle.addons.css') !!}
    {!! Html::style('backend/vendors/css/vendor.bundle.base.css') !!}
    {!! Html::style('backend/vendors/iconfonts/font-awesome/css/font-awesome.min.css') !!}

@endsection
@section('content')

    <style>
        .rate_image{
            width: 50px;
            height: 50px;
            border-radius: 50%;
        }


        #map {
            height: 150px;
            width: 100%;
        }
    </style>
    <!-- Inner Banner -->
    <section id="inner-banner-2">
        <div class="container">
            <div class="row">

                <div class="col-md-12 text-center">
                    <div class="inner_banner_2_detail">
                        <h2>Listing Details</h2>
                        <p><a href="index.html">Home</a> <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                            Listing Details</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Inner Banner -->

    <!-- Listing Details Heading -->
    <section id="listing-details" class="p_b70 p_t70">
        <div class="container">


            <div class="row m_t40">

                <div class="col-md-9 col-sm-9 col-xs-12">


                    <div class="details-heading heading">

                        <h2 class="p_b20">{!! $post->name !!}</h2>


                        <div class="row">

                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="listing-special">
                                    <img style="height: 131px;" src="{!! asset("uploads/posts/$post->main_image") !!}"
                                         alt="image">
                                </div>
                            </div>

                            <div class="col-md-8 col-sm-8 col-xs-12">

                                <div class="listing-special-detail">

                                    {{--<h3 class="p_b20"><span>{!! $post->price_range !!} EGP</span></h3>--}}


                                    <p>{!! html_entity_decode($post->description) !!}</p>

                                    <ul class="listing-amenities">
                                        <li><b>Amenities:</b>
                                        </li>
                                        <br>
                                        <li> Take Reservations : <b
                                                    style="color: #1fb7a6;"> {!! $post->takes_reservations == 1 ? 'Yes': 'no' !!} </b>
                                        </li>
                                        <li> Delivery : <b
                                                    style="color: #1fb7a6;"> {!! $post->delivery == 1 ? 'Yes': 'no' !!} </b>
                                        </li>
                                        <li> Take Away : <b
                                                    style="color: #1fb7a6;"> {!! $post->take_out == 1 ? 'Yes': 'no' !!} </b>
                                        </li>
                                        <li> Accept Credit : <b
                                                    style="color: #1fb7a6;"> {!! $post->acepet_credit == 1 ? 'Yes': 'no' !!} </b>
                                        </li>
                                        <li> Take Reservations : <b
                                                    style="color: #1fb7a6;"> {!! $post->takes_reservations == 1 ? 'Yes': 'no' !!} </b>
                                        </li>
                                        <li> Good For Kids : <b
                                                    style="color: #1fb7a6;"> {!! $post->good_for_kids == 1 ? 'Yes': 'no' !!} </b>
                                        </li>
                                        <li> Wifi : <b
                                                    style="color: #1fb7a6;"> {!! $post->wifi == 1 ? 'Yes': 'No' !!} </b>
                                        </li>

                                    </ul>

                                    <ul class="listing-amenities">
                                        <li><b>Price Range :</b>
                                        </li>
                                        <li>{!! $post->price_range !!} <b style="color: #1fb7a6;"> EGP </b></li>

                                    </ul>

                                    <div class="details-heading-address2">
                                        <ul>

                                            @if(Auth::user())
                                                <li>
                                                    <a href="#write_your_review"><i class="fa fa-star-o"
                                                                                    aria-hidden="true"></i></a>
                                                </li>
                                            @else
                                                <li>
                                                    <a href="{!! url('/login') !!}"><i class="fa fa-star-o"
                                                                                       aria-hidden="true"></i>Login to
                                                        write review</a>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>

                                </div>

                            </div>

                        </div>


                    </div>


                    <div class="details-heading heading">

                        <h2 class="p_b20">{!! count($post->post_rates) !!} Reviews for <span>{!! $post->name !!}</span>
                        </h2>

                        @if(count($post->post_rates) > 0 )

                            @foreach($post->post_rates as $review)
                            <div class="details-heading-review m_t30">

                                <div class="media">

                                <div class="media-left">
                                    @php
                                    $user_image = $review->user->image;
                                    @endphp
                                    <img class="rate_image" src="{!! asset("uploads/users_images/$user_image") !!}" alt="image">
                                </div>

                                <div class="media-body">
                                    <div class="review-detail">


                                        <h3>
                                            {!! $review->user->name !!}
                                            <span>

                                            <div class="my-rating_{!! $review->id !!}"></div>
                                                <em>{!! $review->star_count !!} stars </em>
                                                {{--<img src="{{Request::root()}}/frontend/images/stars-2.png" alt="image">--}}
                                            </span>
                                        </h3>
                                        <span>{!! date('l jS \of F Y ' , strtotime($review->created_at)) !!}</span>
                                        <p>{!! $review->review !!}</p>

                                    </div>
                                </div>

                            </div>

                             </div>
                            @endforeach
                        @endif

                    </div>


                    @if(Auth::user())
                        <div class="details-heading heading" id="write_your_review">

                            <h2 class="p_b20">Write a <span>Review</span></h2>

                            {!! Form::open(['route'=>['review',$post->id] ,  'method' =>'POST' ,'files'=>true]) !!}

                            <div class="row">

                                    <div class="col-md-8">

                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="form-review-message">Review</label>
                                            <textarea class="form-control" id="form-review-message"
                                                      name="review" rows="3" required=""></textarea>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-default">Submit Review</button>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>

                                    <div class="col-md-4">
                                        <aside class="user-rating">
                                            <label>Rate</label>
                                            <select id="example-fontawesome" name="rate" autocomplete="off">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                        </aside>

                                    </div>

                                </div>

                            {!! Form::close() !!}

                        </div>
                    @endif

                </div>

                <div class="col-md-3 col-sm-3 col-xs-12">
                    @if(count($post->post_media))

                        <div class="right-bar bg_white">
                            <h4>Media</h4>
                            <div id="recent-listing" class="owl-carousel owl-theme">

                                @foreach($post->post_media as $media)

                                    <div class="item">
                                        <div class="recent-listing-img">
                                            {{--<a href="{!! asset("uploads/posts/post_media/$media->image") !!}"class="fresco">--}}
                                            <img src="{!! asset("uploads/posts/post_media/$media->image") !!}"
                                                 alt="image">
                                            {{--</a>--}}
                                        </div>

                                    </div>

                                @endforeach
                            </div>
                        </div>
                    @endif
                    <div class="right-bar bg_white">
                        <h4>Get <span>Directions</span></h4>
                        <div id="map"></div>
                        <div id="cd-google-map">
                            {{--<div id="google-container"></div>--}}
                            {{--<div id="cd-zoom-in"></div>--}}
                            {{--<div id="cd-zoom-out"></div>--}}

                            {{--<p><i class="fa fa-map-marker" aria-hidden="true"></i> 57 East 57th Street, New York 10022--}}
                            {{--</p>--}}
                            <p><i class="fa fa-phone" aria-hidden="true"></i>{!! $post->phone !!}</p>
                            <p><i class="fa fa-globe" aria-hidden="true"></i> <a href="#">{!! $post->website !!}</a>
                            </p>
                        </div>
                    </div>


                </div>

            </div>

        </div>
    </section>
    <!-- Listing Details Heading -->

    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

    <script>



        window.onload = function() {

            var latlng = new google.maps.LatLng('{!! $post->latitude !!}' , '{!! $post->longitude !!}');
            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: 'Set lat/lon values for this post',
                draggable: false
            });
            google.maps.event.addListener(marker, 'dragend', function(a) {
                console.log(a);



            });
        };




    </script>


@endsection


@section('scripts')

    {!! Html::script('frontend/star_rating/jquery.star-rating-svg.js') !!}

    @if(count($post->post_rates))


        @foreach($post->post_rates as $review)
            <script>
                $(".my-rating_{!! $review->id !!}").starRating({
                    starSize: 25,
                    callback: function(currentRating, $el){
                        // make a server call here
                    }
                });


            </script>
        <script>
            $('.my-rating_{!! $review->id !!}').starRating('setRating', "{!! $review->star_count !!}")
            $('.my-rating_{!! $review->id !!}').starRating('setReadOnly', true);
        </script>
    @endforeach
    @endif


@endsection

