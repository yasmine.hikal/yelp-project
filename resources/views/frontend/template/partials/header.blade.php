<!DOCTYPE html>
    <html lang="en">
<!-- Mirrored from logicsforest.com/themeforest/find_do/index-5.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 25 May 2018 13:41:40 GMT -->
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
      <title>@yield('page_title')</title>





      {!! Html::style('frontend/css/master.css') !!}



      {{--<link rel="stylesheet" type="text/css" href="css/color-green.css">--}}
      {!! Html::style('frontend/css/color-green.css') !!}
      {{--<link rel="shortcut icon" href="images/short_icon.png">--}}
      {!! Html::style('frontend/images/short_icon.png') !!}


      {!! Html::style('frontend/star_rating/css/star-rating-svg.css') !!}
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>


      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      @yield('styles')
    </head>

    <body>

    {{--<!-- LOADER -->--}}
    {{--<div class="loader">--}}
      {{--<div class="cssload-svg"><img src="{{Request::root()}}/frontend/images/42-3.gif" alt="image">--}}
      {{--</div>--}}
    {{--</div>--}}
    {{--<!--LOADER-->--}}

    <!-- HEADER -->
    <header id="main_header_3">
        <nav class="navbar navbar-default navbar-sticky bootsnav">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Start Header Navigation -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"> <i class="fa fa-bars"></i>
                            </button>
                            <a class="navbar-brand sticky_logo" href="{{url('/')}}"><img src="{{Request::root()}}/frontend/images/logo-footer.png" class="logo" alt="">
                            </a>
                        </div>
                        <!-- End Header Navigation -->
                        <div class="collapse navbar-collapse" id="navbar-menu">
                            <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">

                                <li class="dropdown active"> <a href="{{url('/')}}" class="dropdown" >Home</a>

                                </li>

                                <li class="dropdown"> <a href="#." class="dropdown-toggle" data-toggle="dropdown">Categories</a>
                                    <ul class="dropdown-menu">
                                        @php
                                       $categories=\App\Category::all();
                                        @endphp

                                        @foreach($categories as $category)

                                        <li><a href="{{route('category_posts',$category->id)}}">{{$category->name}}</a>
                                        </li>
                                            @endforeach


                                    </ul>
                                </li>

                                <li class="dropdown"> <a href="{{route('location_search')}}" >Location Search </a>

                                <li class="dropdown"> <a href="{{route('events')}}" >Events</a>

                                </li>


                                <li class="dropdown"> <a href="{{route('contact_us')}}" >Contact Us</a>

                                </li>


                                @if(Auth::user())

                                    <li class="dropdown"> <a href="{{route('profile')}}" >Profile</a>

                                    <li class="dropdown"> <a onclick="document.getElementById('logout-form').submit();" >Logout</a>
                                        <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
                                            {{ csrf_field() }}

                                        </form>
                                @else

                                <li><a href="{{route('login')}}"><i class="fa fa-lock" aria-hidden="true"></i> Login / Register</a> </li>
                                @endif


                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <!-- HEADER  -->