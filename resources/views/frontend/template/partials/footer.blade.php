



<!-- Footer -->
<footer id="footer_1" class="bg_blue p_t70 p_b30">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="footer_logo">
                    <img src="{{Request::root()}}/frontend/images/logo-footer.png" alt="image" />
                </div>
            </div>
            <div class="col-md-6 col-sm-9 col-xs-12 text-center">
                <ul class="footer_link">
                    <li><a href="{{url('/')}}">Home</a>
                    </li>
                    <li><a href="#">Categories</a>
                    </li>
                    <li><a href="{{route('events')}}">Events</a>
                    </li>

                    <li><a href="{{route('contact_us')}}">Contact Us</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="header-top-links">
                    <div class="social-icons text-right">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            </li>
                            <li><a href="#"><i aria-hidden="true" class="fa fa-dribbble"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_line"></div>
    </div>
    <div class="footer_botom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-12 col-sm-12">
                    <p>Copyrights © 2017 Yelp. All rights reserved.</p>
                </div>
                <div class="col-md-6 col-md-12 col-sm-12 text-right">
                    <p>Made with <i class="fa fa-heart fa-1x" style="color: red" aria-hidden="true"></i>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer -->



{{--<script src="js/jquery.2.2.3.min.js"></script>--}}
{!! Html::script('frontend/js/jquery.2.2.3.min.js') !!}
{{--<script src="js/bootstrap.min.js"></script>--}}
{!! Html::script('frontend/js/bootstrap.min.js') !!}
{{--<script src="js/jquery.appear.js"></script>--}}
{!! Html::script('frontend/js/jquery.appear.js') !!}
{{--<script src="js/jquery-countTo.js"></script>--}}
{!! Html::script('frontend/js/jquery-countTo.js') !!}
{{--<script src="js/owl.carousel.min.js"></script>--}}
{!! Html::script('frontend/js/owl.carousel.min.js') !!}
{{--<script src="js/jquery.fancybox.min.js"></script>--}}
{!! Html::script('frontend/js/jquery.fancybox.min.js') !!}
{{--<script src="js/bootsnav.js"></script>--}}
{!! Html::script('frontend/js/bootsnav.js') !!}
{{--<script src="js/zelect.js"></script>--}}
{!! Html::script('frontend/js/zelect.js') !!}
{{--<script src="js/parallax.min.js"></script>--}}
{!! Html::script('frontend/js/parallax.min.js') !!}
{{--<script src="js/modernizr.custom.26633.js"></script>--}}
{!! Html::script('frontend/js/modernizr.custom.26633.js') !!}
{{--<script src="js/jquery.gridrotator.js"></script>--}}
{!! Html::script('frontend/js/jquery.gridrotator.js') !!}



{{--<script src="js/functions.js"></script>--}}
{!! Html::script('frontend/js/functions.js') !!}



<!-- End custom js for this page-->
@yield('scripts')
</body>


<!-- Mirrored from logicsforest.com/themeforest/find_do/index-5.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 25 May 2018 13:41:40 GMT -->
</html>