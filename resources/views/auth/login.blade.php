@extends('frontend.template.app')


@section('page_title' , 'Home page')


@section('content')

    <!-- Inner Banner -->
    <section id="inner-banner-2">
        <div class="container">
            <div class="row">

                <div class="col-md-12 text-center">
                    <div class="inner_banner_2_detail">
                        <h2>Login / Register</h2>
                        <p><a href="index.html">Home</a> <i class="fa fa-angle-double-right" aria-hidden="true"></i> Login / Register</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Inner Banner -->

    <!-- Popular Listing -->
    <section id="login-register" class="p_b70 p_t70">

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#login" aria-controls="login" role="tab" data-toggle="tab">Login</a>
                        </li>
                        <li role="presentation"><a href="#registerd" aria-controls="registerd" role="tab" data-toggle="tab">Registerd</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Tab panes -->
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active" id="login">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="login-register-bg">

                                <div class="row">

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="heading">
                                            <h2>Login</h2>
                                        </div>

                                        <form method="POST" action="{{ route('login') }}" class="registerd" >
                                            @csrf
                                            <div class="form-group">

                                                <input  type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus   placeholder="{{ __('E-Mail Address') }}">

                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback">
                                                  <strong>{{ $errors->first('email') }}</strong>
                                                     </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="{{ __('Password') }}">


                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback">
                                                 <strong>{{ $errors->first('password') }}</strong>
                                                  </span>
                                                @endif
                                            </div>

                                            @if(Session::has('error'))
                                                <div class="form-group  alert alert-danger">

                                                    <p>{!! Session::get('error') !!}</p>

                                                </div>

                                            @endif

                                            <div class="form-group">
                                                <button>Login Now</button>
                                            </div>

                                        </form>
                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane fade" id="registerd">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="login-register-bg">

                                <div class="row">

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="heading">
                                            <h2>Create an <span>account</span></h2>
                                        </div>

                                        <form class="registerd"  method="POST" action="{{ route('register') }}">
                                            @csrf
                                            <div class="form-group" >
                                                <input id="name" type="text" placeholder="Name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <input id="email" placeholder="Email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback">
                                                   <strong>{{ $errors->first('email') }}</strong>
                                                   </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <input id="password" placeholder="Password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback">
                                                   <strong>{{ $errors->first('password') }}</strong>
                                                     </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required>
                                            </div>
                                            <div class="form-group">
                                                <button>Register Now</button>
                                            </div>
                                            <div class="form-group">
                                                <a href="#">Are you a already member ? Login</a>
                                            </div>
                                        </form>
                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
    <!-- Popular Listing -->


@endsection