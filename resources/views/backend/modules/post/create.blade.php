@extends('backend.layout')

@section('page_title' , 'Posts')


@section('styles')

    <style>
        #map {
           height: 400px;
           width: 100%;
        }


        .hidden{
            display: none ; 
        }
     </style>
 

@endsection

@section('content')

 <div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Add new post</h4>
        <p class="card-description">
          use the below form to create a new post , please fill in the below fields
        </p>

          @if (count($errors->all()) > 0 )
              <ul class="alert alert-danger" style="list-style:none;">
                  @foreach ($errors->all() as $error)
                      <li>{!! $error !!}</li>
                  @endforeach
              </ul>
          @endif

     {!! Form::open(['route'=>['post.store'] ,  'method' =>'POST' ,'files'=>true]) !!}




          <div class="form-group">
            <label>Name</label>
            <input type="text" name="name" class="form-control" placeholder="post name">
          </div>
          <div class="form-group">
            <label>Description</label>
            
            <textarea id="summernote" name="description"></textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword4">Post Main Image</label>
            <input type="file" name="main_image" class="dropify">
           
          </div>


          <div class="form-group">
                <label for="exampleInputPassword4">Post attachements</label>
                <input type="file" name="post_media[]" id="filer_input"  multiple> 
               
              </div>

          <div class="form-group">
              <label>Phone</label>
              <input type="text" name="phone" class="form-control" placeholder="phone">
          </div>

          <div class="form-group">
              <label>Website</label>
              <input type="text" name="website" class="form-control" placeholder="Website URL">
          </div>

          <div class="form-group">
              <label>Price Range</label>
              <input type="text" name="price_range" class="form-control" placeholder="Price Range">
          </div>

          <div class="form-group">
              <label>Opening From</label>
              <input type="text" name="opening_from" class="form-control" placeholder="Opening From">
          </div>

          <div class="form-group">
              <label>Opening To</label>
              <input type="text" name="opening_to" class="form-control" placeholder="Opening To ">
          </div>
          
          
           
              <label>Is Restaurant ? </label>
              
              <div class="icheck-flat">
                  <input tabindex="5" type="checkbox" name="is_restaurant" id="is_restaurant">
                    {{-- <input tabindex="5" type="checkbox" name="is_restaurant" id="is_restaurant"> --}}
                    <label for="flat-checkbox-1">Restaurant Post</label>
              </div>
              <br>
           

          <div id="options" class="hidden">
                <div class="icheck-line">
                    <input tabindex="5" type="checkbox" name="takes_reservations" id="line-checkbox-1">
                    <label for="line-checkbox-1">Take Reservations</label>
                </div>
                <div class="icheck-line">
                    <input tabindex="6" type="checkbox" name="delivery" id="line-checkbox-2">
                    <label for="line-checkbox-2">Offers Delivery</label>
                </div>
                <div class="icheck-line">
                    <input tabindex="6" type="checkbox" name="take_out" id="line-checkbox-2">
                    <label for="line-checkbox-2">Support take out food</label>
                </div>
                <div class="icheck-line">
                    <input tabindex="6" type="checkbox" name="acepet_credit"  id="line-checkbox-2">
                    <label for="line-checkbox-2">Accepts credit cards </label>
                </div>
                <div class="icheck-line">
                    <input tabindex="6" type="checkbox" name="good_for_kids" id="line-checkbox-2">
                    <label for="line-checkbox-2">Restaurant suitable for kids</label>
                </div>
                <div class="icheck-line">
                    <input tabindex="6" type="checkbox" name="wifi" id="line-checkbox-2">
                    <label for="line-checkbox-2">Support Wifi</label>
                </div>
          </div>





          <div class="form-group">
                <label for="exampleFormControlSelect2">Categories</label>
                <select class="form-control" name="category_id" id="exampleFormControlSelect2">
                   @foreach($Categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                   @endforeach
                </select>
            </div>
            <div class="form-group">
                <label >Publish / Draft</label>
                <div class="icheck-line">
                    <input tabindex="7" type="radio" id="line-radio-1" value="1" name="status" checked>
                    <label for="line-radio-1">Publish this post</label>
                </div>
                <div class="icheck-line">
                    <input tabindex="8" type="radio" id="line-radio-2" value="0" name="status" >
                    <label for="line-radio-2">Draft this post </label>
                </div>
            </div>

            <label for="" style="margin-top:5px;">Location</label>
            <div class="card-body col-md-12">

                <div id="map"></div>
                <input type="hidden" placeholder="{{ trans('backend.latitude') }}"  value="27.276551611306182"  name="latitude"  class="form-control form-control-line" id="latiude">
                <input type="hidden" placeholder="{{ trans('backend.longitude') }}" value="30.703368206249934" name="longitude"  class="form-control form-control-line" id="longitude">

            </div>
  
            <button type="submit" class="btn btn-success mr-2">Submit</button>
            <button class="btn btn-light">Cancel</button>

          {!! Form::close() !!}

      </div>
    </div>
  </div>
 </div>

@endsection


@section('scripts')

<script>
  // for image input
  jQuery(document).ready(function() {

     $('.dropify').dropify();
     $('#summernote').summernote({
        placeholder: 'Hello yelp admin , enjoy cruding',
        tabsize: 2,
        height: 200
      });


  });



</script>

{!! Html::script('backend/js/formpickers.js') !!}

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

<script>
            
 

    window.onload = function() {

        var latlng = new google.maps.LatLng(27.276551611306182 , 30.703368206249934);
        var map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            zoom: 5,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: 'Set lat/lon values for this post',
            draggable: true
        });
        google.maps.event.addListener(marker, 'dragend', function(a) {
            console.log(a);

            $('#latiude').val(a.latLng.lat()) ; 
            $('#longitude').val(a.latLng.lng()) ; 
           
        });
    };

      


</script>


<script>
    // special script for checking un checking is restaurant option  -_- 
 

        $('input#is_restaurant').on('ifChecked', function(event){
            $('#options').fadeIn("slow");  
        });
        $('input#is_restaurant').on('ifUnchecked', function(event){
            $('#options').fadeOut("slow");      
        });

   
</script>


@endsection