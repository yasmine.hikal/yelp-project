@extends('backend.layout')

@section('page_title' , 'post')

@section('content')

      @if (Session::has('post_added'))
                  
      <div class="alert alert-fill-success"> 
              <p>   <i class="mdi mdi-alert-circle"></i>  {!! Session::get('post_added') !!}</p>
      </div>




      @endif

      @if (Session::has('post_updated'))

      <div class="alert alert-fill-success">
              <p>   <i class="mdi mdi-alert-circle"></i>  {!! Session::get('post_updated') !!}</p>
      </div>




      @endif
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Posts</h4>
          <div class="row">
            <div class="col-12">
              <table id="order-listing" class="table">
                <thead>
                <tr>
                  <th>#</th>
                  <th>name</th>
                  <th>category</th>
                  <th>image</th>
                  <th>open from</th>
                  <th>open to</th>
                  <th>status</th>
                  <th>actions</th>
                </tr>
                </thead>
                <tbody>
                @if(count($posts)>0)
                  @foreach($posts as $post)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$post->name}}</td>
                  <td> 
                      <div class="badge badge-outline-primary">{{ $post->category->name}}</div>
                  </td>
                  <td>
                    <img src="{{ asset("uploads/posts/$post->main_image") }}" class="img-lg rounded-circle mb-2" alt="profile image">
                  </td>
                <td>
                  {{ $post->opening_from }}
                </td>
                <td>
                  {{ $post->opening_to }}
                </td>
                <td>
                  <div class="badge badge-{!! $post->status == '1' ? 'success' : 'danger' !!}"> 
                      {!! $post->status == '1' ? 'Published' : 'Drafted' !!}
                  </div>
                </td>
                  <td >

                    <a  href="{{route('post.edit',$post->id)}}" > <i class="fa fa-edit text-danger"></i> </a>


                    <a  onclick="$('.post_form_{{$post->id}}').submit();" > <i class="fa fa-close text-info"></i> </a>

                    {!! Form::open(['route'=>['post.destroy', $post->id] , 'class'=>"post_form_$post->id" , 'method' =>'POST']) !!}

                    {!! method_field('DELETE') !!}


                    {!! Form::close() !!}
                  </td>
                </tr>
                @endforeach
                @endif

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>


@endsection


