@extends('backend.layout')

@section('page_title' , 'Edit Post')


@section('styles')

    <style>
        #map {
            height: 400px;
            width: 100%;
        }


        .hidden{
            display: none ;
        }
    </style>


@endsection

@section('content')

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Edit post</h4>
                    <p class="card-description">
                        use the below form to Edit post , please fill in the below fields
                    </p>

                    @if (count($errors->all()) > 0 )
                        <ul class="alert alert-danger" style="list-style:none;">
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model( $post ,  ['route'=>['post.update', $post->id ] ,  'method' =>'POST' ,'files'=>true]) !!}

                        {!! method_field('put') !!}


                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" placeholder="post name" value="{!! $post->name !!}">
                    </div>
                    <div class="form-group">
                        <label>Description</label>

                        <textarea id="summernote" name="description">{!! $post->description !!}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword4">Post Main Image</label>
                        <input type="file" name="main_image" class="dropify">

                    </div>


                    <div class="form-group">
                        <label for="exampleInputPassword4">Post attachements</label>
                        <input type="file" name="post_media[]" id="filer_input"  multiple>

                    </div>


                    <div class="form-group">

                        <div class="col-md-12 h-100">
                            <div class="bg-secondary p-4">
                                <h6 class="card-title">Old Attachements</h6>
                                <div class="alert alert-success hidden" id="msg_div">
                                    Image attachment has been deleted successfully
                                </div>
                                <div id="profile-list-left" class="py-2">
                                   @if(count($post->post_media))
                                       @foreach($post->post_media as $image)
                                        <div class="card rounded mb-2">
                                        <div class="card-body p-3">
                                            <div class="media">
                                                <img src="{!! asset("uploads/posts/post_media/$image->image") !!}" alt="image" class="img-sm mr-3 rounded-circle">

                                                <div class="media-body">
                                                    <h6 class="mb-1">{!! $image->name !!}</h6>
                                                    <p class="mb-0 text-muted">
                                                        {!! date('Y - m - d' , strtotime($image->created_at)) !!}
                                                    </p>

                                                </div>
                                                <button class="btn btn-danger btn-sm pull-right delete_image" data-image-id="{!! $image->id !!}">Delete</button>
                                            </div>
                                        </div>
                                        </div>
                                       @endforeach

                                   @else

                                        <code>No attachments found for this post</code>

                                   @endif

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" name="phone" class="form-control" placeholder="phone" value="{!! $post->phone !!}">
                    </div>

                    <div class="form-group">
                        <label>Website</label>
                        <input type="text" name="website" class="form-control" placeholder="Website URL" value="{!! $post->website !!}">
                    </div>

                    <div class="form-group">
                        <label>Price Range</label>
                        <input type="text" name="price_range" class="form-control" placeholder="Price Range" value="{!! $post->price_range !!}">
                    </div>

                    <div class="form-group">
                        <label>Opening From</label>
                        <input type="text" name="opening_from" class="form-control" placeholder="Opening From" value="{!! $post->opening_from !!}">
                    </div>

                    <div class="form-group">
                        <label>Opening To</label>
                        <input type="text" name="opening_to" class="form-control" placeholder="Opening To " value="{!! $post->opening_to !!}">
                    </div>



                    <label>Is Restaurant ? </label>

                    <div class="icheck-flat">
                        <input tabindex="5" disabled type="checkbox" name="is_restaurant" id="is_restaurant" {!! $post->is_restaurant == '1' ? 'checked':'' !!}>
                        {{-- <input tabindex="5" type="checkbox" name="is_restaurant" id="is_restaurant"> --}}
                        <label for="flat-checkbox-1">Restaurant Post</label>
                    </div>
                    <br>


                    <div id="options" class="hidden">
                        <div class="icheck-line">
                            <input tabindex="5" type="checkbox" name="takes_reservations" id="line-checkbox-1" {!! $post->takes_reservations == '1' ? 'checked':'' !!} >
                            <label for="line-checkbox-1">Take Reservations</label>
                        </div>
                        <div class="icheck-line">
                            <input tabindex="6" type="checkbox" name="delivery" id="line-checkbox-2" {!! $post->delivery == '1' ? 'checked':'' !!}>
                            <label for="line-checkbox-2">Offers Delivery</label>
                        </div>
                        <div class="icheck-line">
                            <input tabindex="6" type="checkbox" name="take_out" id="line-checkbox-2" {!! $post->take_out == '1' ? 'checked':'' !!}>
                            <label for="line-checkbox-2">Support take out food</label>
                        </div>
                        <div class="icheck-line">
                            <input tabindex="6" type="checkbox" name="acepet_credit"  id="line-checkbox-2" {!! $post->acepet_credit == '1' ? 'checked':'' !!}>
                            <label for="line-checkbox-2">Accepts credit cards </label>
                        </div>
                        <div class="icheck-line">
                            <input tabindex="6" type="checkbox" name="good_for_kids" id="line-checkbox-2" {!! $post->good_for_kids == '1' ? 'checked':'' !!}>
                            <label for="line-checkbox-2">Restaurant suitable for kids</label>
                        </div>
                        <div class="icheck-line">
                            <input tabindex="6" type="checkbox" name="wifi" id="line-checkbox-2" {!! $post->wifi == '1' ? 'checked':'' !!}>
                            <label for="line-checkbox-2">Support Wifi</label>
                        </div>
                    </div>





                    <div class="form-group">
                        <label for="exampleFormControlSelect2">Categories</label>
                        <select class="form-control" name="category_id" id="exampleFormControlSelect2">
                            @foreach($Categories as $category)
                                <option value="{{$category->id}}" {!! $category->id == $post->category_id ? 'selected' :'' !!}>{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label >Publish / Draft</label>
                        <div class="icheck-line">
                            <input tabindex="7" type="radio" id="line-radio-1" value="1" name="status" {!! $post->status == '1' ? 'checked':'' !!}>
                            <label for="line-radio-1">Publish this post</label>
                        </div>
                        <div class="icheck-line">
                            <input tabindex="8" type="radio" id="line-radio-2" value="0" name="status" {!! $post->status == '0' ? 'checked':'' !!}>
                            <label for="line-radio-2">Draft this post </label>
                        </div>
                    </div>

                    <label for="" style="margin-top:5px;">Location</label>
                    <div class="card-body col-md-12">

                        <div id="map"></div>
                        <input type="hidden" placeholder="{{ trans('backend.latitude') }}"  value="{!! $post->latitude !!}"  name="latitude"  class="form-control form-control-line" id="latiude">
                        <input type="hidden" placeholder="{{ trans('backend.longitude') }}" value="{!! $post->longitude !!}" name="longitude"  class="form-control form-control-line" id="longitude">

                    </div>

                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts')

    <script>
        // for image input
        jQuery(document).ready(function() {

            $('.dropify').dropify();
            $('#summernote').summernote({
                placeholder: 'Hello yelp admin , enjoy cruding',
                tabsize: 2,
                height: 200
            });


        });



    </script>

    {!! Html::script('backend/js/formpickers.js') !!}

    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

    <script>



        window.onload = function() {

            var latlng = new google.maps.LatLng(27.276551611306182 , 30.703368206249934);
            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 5,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: 'Set lat/lon values for this post',
                draggable: true
            });
            google.maps.event.addListener(marker, 'dragend', function(a) {
                console.log(a);

                $('#latiude').val(a.latLng.lat()) ;
                $('#longitude').val(a.latLng.lng()) ;

            });
        };




    </script>


    <script>
        // special script for checking un checking is restaurant option  -_-
        if($('input#is_restaurant').prop('checked') == true){

            $('input#is_restaurant').prop('checked' , function(event){
                $('#options').fadeIn("slow");
            });

        }




        $('input#is_restaurant').on('ifUnchecked', function(event){
            $('#options').fadeOut("slow");
        });

        $('.delete_image').on('click' , function(e){
            e.preventDefault();
           var image_record = $(this).attr('data-image-id');

           var element  = $(this).parent().parent();

           $.ajax({
               method:'GET',
               url:"{!! url('delete_image') !!}",
               dataType:'json' ,
               data: {image_id:image_record},
               beforeSend:function(){

               },
               success:function(data){

                   if(data.status == 'success'){

                        // means im here now  -_-
                        $('#msg_div').fadeIn('slow');
                        element.fadeOut('slow');

                   }
               },
               complete:function(){

                        $('#msg_div').fadeIn('slow');
               }
           });
           return false ;
        });


    </script>


@endsection