@extends('backend.layout')

@section('page_title' , 'Events')


@section('styles')
    
    <style>
       #map {
          height: 400px;
          width: 100%;
       }
    </style>


@endsection

@section('content')

 <div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Add new event</h4>
        <p class="card-description">
          use the below form to create a new event , please fill in the below fields
        </p>

          @if (count($errors->all()) > 0 )
              <ul class="alert alert-danger" style="list-style:none;">
                  @foreach ($errors->all() as $error)
                      <li>{!! $error !!}</li>
                  @endforeach
              </ul>
          @endif

     {!! Form::open(['route'=>['event.store'] ,  'method' =>'POST' ,'files'=>true]) !!}




          <div class="form-group">
            <label>Name</label>
            <input type="text" name="name" class="form-control" placeholder="event name">
          </div>
          <div class="form-group">
            <label>Description</label>
            
            <textarea id="summernote" name="description"></textarea>
          </div>
          
          <div class="form-group">
            <label for="exampleInputPassword4">event image</label>
            <input type="file" name="image" class="dropify">
           
          </div>
            <div class="row">
                <div class="col-md-6">
                    <label>From</label>
                    {{-- <h4 class="card-title">From</h4> --}}
                    <div id="datepicker-popup" class="input-group date datepicker ">
                        <input type="text" name="from" class="form-control">
                        <span class="input-group-addon input-group-append border-left">
                              <span class="mdi mdi-calendar input-group-text"></span>
                            </span>
                    </div>
                </div>
              <div class="col-md-6">
                    {{-- <h4 class="card-title">To</h4> --}}
                    <label>To</label>
                    <div id="inline-datepicker" class="input-group date datepicker ">
                        <input type="text" name="to" class="form-control">
                        <span class="input-group-addon input-group-append border-left">
                              <span class="mdi mdi-calendar input-group-text"></span>
                            </span>
                    </div>
                </div>
            </div>

            
          

            
            <label for="" style="margin-top:5px;">Event Location</label>
            <div class="card-body col-md-12">
                {{-- <h4 class="card-title">Event Location</h4> --}}
               
                  <!-- used for geolocation --> 
                  <div id="map"></div>
                
                          <input type="hidden" placeholder="{{ trans('backend.latitude') }}"  value="27.276551611306182"  name="latitude"  class="form-control form-control-line" id="latiude">
                          <input type="hidden" placeholder="{{ trans('backend.longitude') }}" value="30.703368206249934" name="longitude"  class="form-control form-control-line" id="longitude">


              </div>




          
          <button type="submit" class="btn btn-success mr-2">Submit</button>
          <button class="btn btn-light">Cancel</button>
          {!! Form::close() !!}
      </div>
    </div>
  </div>
 </div>

@endsection


@section('scripts')

<script>
  // for image input
  jQuery(document).ready(function() {

     $('.dropify').dropify();
     $('#summernote').summernote({
        placeholder: 'Hello yelp admin , enjoy cruding',
        tabsize: 2,
        height: 200
      });


  });



</script>

{!! Html::script('backend/js/formpickers.js') !!}

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

<script>
            
 

    window.onload = function() {
        var latlng = new google.maps.LatLng(27.276551611306182 , 30.703368206249934);
        var map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: 'Set lat/lon values for this property',
            draggable: true
        });
        google.maps.event.addListener(marker, 'dragend', function(a) {
            console.log(a);
            // var div = document.createElement('div');
            // div.innerHTML = a.latLng.lat().toFixed(4) + ', ' + a.latLng.lng().toFixed(4);


            $('#latiude').val(a.latLng.lat()) ; 
            $('#longitude').val(a.latLng.lng()) ; 
           
        });
    };

      


</script>


@endsection