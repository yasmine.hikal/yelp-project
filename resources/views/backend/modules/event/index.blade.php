@extends('backend.layout')

@section('page_title' , 'events')

@section('content')

      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Events</h4>
          <div class="row">
            <div class="col-12">
              <table id="order-listing" class="table">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Image</th>
                  <th>Added Date</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if(count($events)>0)
                  @foreach($events as $event)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$event->name}}</td>
                  <td>{{strip_tags($event->description)}}</td>
                  <td>
                    <img src="{{Request::root()}}/uploads/events_images/{{$event->image}}" class="img-lg rounded-circle mb-2" alt="profile image">
                  </td>
                <td>
                  {{ date('Y-m-d' , strtotime($event->created_at)) }}
                </td>
                  <td >

                    <a  href="{{route('event.edit',$event->id)}}" > <i class="fa fa-edit text-danger"></i> </a>


                    <a  onclick="$('.event_form_{{$event->id}}').submit();" > <i class="fa fa-close text-info"></i> </a>

                    {!! Form::open(['route'=>['event.destroy', $event->id] , 'class'=>"event_form_$event->id" , 'method' =>'POST']) !!}

                    {!! method_field('DELETE') !!}


                    {!! Form::close() !!}
                  </td>
                </tr>
                @endforeach
                @endif

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>


@endsection


