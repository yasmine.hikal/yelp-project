@extends('backend.layout')

@section('page_title' , 'Users')

@section('content')

      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Users</h4>
          <div class="row">
            <div class="col-12">
              <table id="order-listing" class="table">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Image</th>
                  <th>Added Date</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if(count($users)>0)
                  @foreach($users as $user)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$user->name}}</td>
                  <td>{{$user->email}}</td>
                  <td>
                    <img src="{{Request::root()}}/uploads/users_images/{{$user->image}}" class="img-lg rounded-circle mb-2" alt="profile image">
                  </td>
                <td>
                  {{ date('Y-m-d' , strtotime($user->created_at)) }}
                </td>
                  <td >

                    <a  onclick="$('.user_form_{{$user->id}}').submit();" > <i class="fa fa-close text-danger"></i> </a>

                    {!! Form::open(['route'=>['user.destroy', $user->id] , 'class'=>"user_form_$user->id" , 'method' =>'POST']) !!}

                    {!! method_field('DELETE') !!}


                    {!! Form::close() !!}
                  </td>
                </tr>
                @endforeach
                @endif

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>


@endsection


