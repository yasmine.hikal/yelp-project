@extends('backend.layout')

@section('page_title' , 'Dashboard Home')

@section('content')

  <div class="row">
    <div class="col-md-4 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Admin Dept</h4>
          <div class="w-50 mt-5 mb-4 mx-auto">
            <div id="revenueCircle3" class="progressbar-js-circle"></div>
          </div>
          <h4 class="text-center"><strong>Storage Size</strong></h4>
          <h4 class="text-center"><strong>1.98TB</strong></h4>
          <div class="d-flex row mt-5">
            <div class="col">
              <p class="text-left mb-2">1.30 GB free</p>
              <h4 class="text-left"><strong>35.4%</strong></h4>
            </div>
            <div class="col">
              <p class="text-right mb-2">1.30 GB free</p>
              <h4 class="text-right"><strong>35.4%</strong></h4>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-8 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Recent Updates</h4>
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th>Asigned Name</th>
                <th>Progress</th>
                <th>Amount</th>
                <th>Deadline</th>
              </tr>
              <tr>
                <td>
                  <div class="d-flex align-items-center">
                    <div><img src="images/faces/face2.jpg" alt="profile image"></div>
                    <div class="ml-3">
                      <p class="mb-1">Jessica T. Phillips</p>
                      <small class="text-muted">Sales Assistant</small>
                    </div>
                  </div>
                </td>
                <td><canvas id="areaChart_1" style="height:30px; max-width:130px;"></canvas></td>
                <td>$450.12</td>
                <td>Mar 08 2018</td>
              </tr>
              <tr>
                <td>
                  <div class="d-flex align-items-center">
                    <div><img src="images/faces/face1.jpg" alt="profile image"></div>
                    <div class="ml-3">
                      <p class="mb-1">Luke J. Sain</p>
                      <small class="text-muted">Software Engineer</small>
                    </div>
                  </div>
                </td>
                <td><canvas id="areaChart_2" style="height:30px; max-width:130px;"></canvas></td>
                <td>$124.66</td>
                <td>Mar 09 2018</td>
              </tr>
              <tr>
                <td>
                  <div class="d-flex align-items-center">
                    <div><img src="images/faces/face4.jpg" alt="profile image"></div>
                    <div class="ml-3">
                      <p class="mb-1">Mark C. Diaz</p>
                      <small class="text-muted">Accountant</small>
                    </div>
                  </div>
                </td>
                <td><canvas id="areaChart_3" style="height:30px; max-width:130px;"></canvas></td>
                <td>$763.00</td>
                <td>Mar 10 2018</td>
              </tr>
              <tr>
                <td>
                  <div class="d-flex align-items-center">
                    <div><img src="images/faces/face6.jpg" alt="profile image"></div>
                    <div class="ml-3">
                      <p class="mb-1">Margeret V. Ligon</p>
                      <small class="text-muted">Software Engineer</small>
                    </div>
                  </div>
                </td>
                <td><canvas id="areaChart_4" style="height:30px; max-width:130px;"></canvas></td>
                <td>$120.76</td>
                <td>Mar 11 2018</td>
              </tr>
              <tr>
                <td>
                  <div class="d-flex align-items-center">
                    <div><img src="images/faces/face8.jpg" alt="profile image"></div>
                    <div class="ml-3">
                      <p class="mb-1">Messy max</p>
                      <small class="text-muted">Personnel Lead</small>
                    </div>
                  </div>
                </td>
                <td><canvas id="areaChart_5" style="height:30px; max-width:130px;"></canvas></td>
                <td>$450.20</td>
                <td>Mar 12 2018</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
