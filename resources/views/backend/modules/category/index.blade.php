@extends('backend.layout')

@section('page_title' , 'Category listing')

@section('content')
    
      <div class="card">

        @if (Session::has('cat_added'))
            
            <div class="alert alert-fill-success">
                  
                    <p>   <i class="mdi mdi-alert-circle"></i>  {!! Session::get('cat_added') !!}</p>
            </div>

        @endif
        @if (Session::has('cat_updated'))
            
            <div class="alert alert-fill-success">
                  
                    <p>   <i class="mdi mdi-alert-circle"></i> {!! Session::get('cat_updated') !!}</p>
            </div>

        @endif

        <div class="card-body">
          <h4 class="card-title">Categories</h4>
          <div class="row">
            <div class="col-12">
              <table id="order-listing" class="table">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  {{-- <th>Description</th> --}}
                  <th>Added Date</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if(count($categories)>0)
                  @foreach($categories as $category)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$category->name}}</td>
                  {{-- <td>{{ str_limit($category->description , 50  )  }}</td> --}}
                <td>
                  {{ date('l jS \of F Y h:i:s A' , strtotime($category->created_at)) }}
                </td>
                  <td >
                        <a href="{!! route('category.edit' , $category->id) !!}"> <i class="fa fa-edit text-primary"></i> </a>
                    {{-- <a style="cursor:pointer;"   onclick="$('.categories_form_{{$category->id}}').submit();" > <i class="fa fa-close text-danger"></i> </a>

                    {!! Form::open(['route'=>['category.destroy', $category->id] , 'class'=>"categories_form_$category->id" , 'method' =>'POST']) !!}

                    {!! method_field('DELETE') !!}


                    {!! Form::close() !!} --}}

                  </td>
                </tr>
                @endforeach
                @endif

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>


@endsection


