@extends('backend.layout')

@section('page_title' , 'Create category')


@section('styles')



@endsection

@section('content')

 <div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Add new category</h4>
        <p class="card-description">
          use the below form to create a new category , please fill in the below fields
        </p>

        @if (count($errors->all()) > 0 )
          <ul class="alert alert-danger" style="list-style:none;">
            @foreach ($errors->all() as $error)
                <li>{!! $error !!}</li>
            @endforeach
          </ul>
        @endif


        {!! Form::open(['route'=>'category.store' , 'class'=>'forms-sample' , 'method'
        =>'POST' , 'files'=>true]) !!}
        
          <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="cat_name" placeholder="category name">
          </div>
          <div class="form-group">
            <label>Description</label>
            
            <textarea id="summernote" name="cat_description"></textarea>
          </div>
          
          <div class="form-group">
            <label for="exampleInputPassword4">Category image</label>
            <input type="file" name="cat_image" class="dropify"> 
           
          </div>
						
          <div class="form-group">
            <label for="exampleInputCity1">Icon</label>
            <input type="text" class="form-control" name="cat_icon" placeholder="category icon">
          </div>
          
          <button type="submit" class="btn btn-success mr-2">Submit</button>
           
        {!! Form::close() !!}
      </div>
    </div>
  </div>
 </div>

@endsection


@section('scripts')

<script>
  // for image input
  jQuery(document).ready(function() {

     $('.dropify').dropify();
     $('#summernote').summernote({
        placeholder: 'Hello yelp admin , enjoy cruding',
        tabsize: 2,
        height: 200
      });


  });



</script>

@endsection