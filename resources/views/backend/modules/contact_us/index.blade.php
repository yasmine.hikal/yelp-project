@extends('backend.layout')

@section('page_title' , 'Category listing')

@section('content')
    
      <div class="card">
        
        <div class="card-body">
          <h4 class="card-title">Categories</h4>
          <div class="row">
            <div class="col-12">
              <table id="order-listing " class="table responsive">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                   <th>phone</th> 
                   <th>email</th> 
                   <th>message</th> 
                  <th>Added Date</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if(count($contacts)>0)
                  @foreach($contacts as $contact)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$contact->name}}</td>
                  <td>{{$contact->phone}}</td>
                  <td>{{$contact->email}}</td>
                  <td>{{$contact->message}}</td>


                  {{-- <td>{{ str_limit($contact->description , 50  )  }}</td> --}}
                <td>
                  {{ date('l jS \of F Y h:i:s A' , strtotime($contact->created_at)) }}
                </td>
                  <td >
                     <a style="cursor:pointer;"   onclick="$('.categories_form_{{$contact->id}}').submit();" > <i class="fa fa-close text-danger"></i> </a>

                    {!! Form::open(['route'=>['contact.destroy', $contact->id] , 'class'=>"categories_form_$contact->id" , 'method' =>'POST']) !!}

                    {!! method_field('DELETE') !!}


                    {!! Form::close() !!}

                  </td>
                </tr>
                @endforeach
                @endif

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>


@endsection


