</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- plugins:js -->
{{-- <script src="vendors/js/vendor.bundle.base.js"></script> --}}
{!! Html::script('backend/vendors/js/vendor.bundle.base.js') !!}
{{-- <script src="vendors/js/vendor.bundle.addons.js"></script> --}}
{!! Html::script('backend/vendors/js/vendor.bundle.addons.js') !!}


<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
{{-- <script src="js/off-canvas.js"></script> --}}
{!! Html::script('backend/js/off-canvas.js') !!}

{{ Html::script('backend/dropify/dist/js/dropify.min.js') }} 

{!! Html::script('backend/jquery.filer/js/jquery.filer.min.js') !!} 
{!! Html::script('backend/filer/jquery.fileuploads.init.js') !!}
{!! Html::script('backend/filer/custom-filer.js') !!}

{{-- <script src="js/hoverable-collapse.js"></script> --}}
{!! Html::script('backend/js/hoverable-collapse.js') !!}
{{-- <script src="js/misc.js"></script> --}}
{!! Html::script('backend/js/misc.js') !!}
{{-- <script src="js/settings.js"></script> --}}
{!! Html::script('backend/js/settings.js') !!}
 
{!! Html::script('backend/js/iCheck.js') !!}

 
{!! Html::script('backend/js/typeahead.js') !!}
{!! Html::script('backend/js/select2.js') !!}


{!! Html::script('backend/vendors/summernote/dist/summernote-bs4.min.js') !!}




{{-- <script src="js/todolist.js"></script> --}}
{!! Html::script('backend/js/todolist.js') !!}
<!-- endinject -->
<!-- Custom js for this page-->
{{-- <script src="js/dashboard.js"></script> --}}
{!! Html::script('backend/js/dashboard.js') !!}


<!-- Custom js for this page-->

{!! Html::script('backend/js/data-table.js') !!}
<!-- End custom js for this page-->
<!-- End custom js for this page-->
{!! Html::script('backend/js/js-grid.js') !!}
{!! Html::script('backend/js/db.js') !!}
<!-- End custom js for this page-->
@yield('scripts')

</body>


</html>
