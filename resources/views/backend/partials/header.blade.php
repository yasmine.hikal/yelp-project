<!DOCTYPE html>
<html lang="en">


<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>@yield('page_title')</title>
  <!-- plugins:css -->
  {{-- <link rel="stylesheet" href="vendors/iconfonts/mdi/css/materialdesignicons.min.css"> --}}
  {!! Html::style('backend/vendors/iconfonts/mdi/css/materialdesignicons.min.css') !!}

    
  {{-- <link rel="stylesheet" href="vendors/iconfonts/puse-icons-feather/feather.css"> --}}
  {!! Html::style('backend/vendors/iconfonts/puse-icons-feather/feather.css') !!}
  {{-- <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css"> --}}
  {!! Html::style('backend/vendors/css/vendor.bundle.base.css') !!}
  {{-- <link rel="stylesheet" href="vendors/css/vendor.bundle.addons.css"> --}}
  {!! Html::style('backend/vendors/css/vendor.bundle.addons.css') !!}
  <!-- endinject -->
  <!-- plugin css for this page -->
  {{-- <link rel="stylesheet" href="vendors/iconfonts/font-awesome/css/font-awesome.min.css" /> --}}
  {!! Html::style('backend/vendors/iconfonts/font-awesome/css/font-awesome.min.css') !!}

    <!-- dropfy -->       
{{ Html::style('backend/dropify/dist/css/dropify.min.css') }}

{!! Html::style('backend/jquery.filer/css/jquery.filer.css') !!} 
{!! Html::style('backend/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css') !!} 


<!-- include summernote css/js -->

  
{!! Html::style('backend/vendors/summernote/dist/summernote-bs4.css') !!}

 
{!! Html::style('backend/vendors/icheck/skins/all.css') !!}




  <!-- End plugin css for this page -->
  <!-- inject:css -->
  {{-- <link rel="stylesheet" href="css/style.css"> --}}
  {!! Html::style('backend/css/style.css') !!}



  @yield('styles')
  <!-- endinject -->
  <link rel="shortcut icon" href="{!! asset("backend/images/favicon.png") !!}" />
</head>
<body>
  <div class="container-scroller">
