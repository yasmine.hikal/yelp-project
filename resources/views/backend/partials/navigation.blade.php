<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row navbar-success">
  <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
    <a class="navbar-brand brand-logo" href="{{url('/')}}"><img src="{{Request::root()}}/frontend/images/logo-footer1.png" class="logo" alt=""></a>
    <a class="navbar-brand brand-logo-mini" href="{{url('/')}}"><img src="{{Request::root()}}/frontend/images/logo-footer1.png" class="logo" alt=""></a>
  </div>
  <div class="navbar-menu-wrapper d-flex align-items-stretch">
    <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
      <span class="mdi mdi-menu"></span>
    </button>
    <ul class="navbar-nav">
      <li class="nav-item d-none d-lg-block">
        <a class="nav-link">
          <i class="mdi mdi-fullscreen" id="fullscreen-button"></i>
        </a>
      </li>

    </ul>
    <ul class="navbar-nav navbar-nav-right">


      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle nav-profile" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
          <img src="images/faces/face1.jpg" alt="image">
          <span class="d-none d-lg-inline">Admin</span>
        </a>
        <div class="dropdown-menu navbar-dropdown w-100" aria-labelledby="profileDropdown">

          <div class="dropdown-divider"></div>
          <a class="dropdown-item" style="cursor:pointer;" onclick="document.getElementById('logout-form').submit();">
            <i class="mdi mdi-logout mr-2 text-primary"></i>
            Signout
          </a>
          <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
            {{ csrf_field() }}

          </form>
        </div>
      </li>

    </ul>
    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
    <span class="mdi mdi-menu"></span>
  </button>
  </div>
</nav>
