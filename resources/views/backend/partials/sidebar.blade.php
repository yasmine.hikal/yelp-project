<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-profile">
      <div class="nav-link d-flex">
        <div class="profile-image">
          <img src="{{Request::root()}}/user_main_image.png" alt="image"/>
          <span class="online-status online"></span> <!--change class online to offline or busy as needed-->
        </div>
        <div class="profile-name">
          <p class="name">
            Admin
          </p>

        </div>
      </div>
    </li>


    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#page-layouts" aria-expanded="false" aria-controls="page-layouts">
        <i class="icon-box menu-icon"></i>
        <span class="menu-title">Users</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="page-layouts">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{Route('user.index')}}">Show All Users</a></li>

        </ul>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#page-layout1" aria-expanded="false" aria-controls="page-layouts">
        <i class="icon-box menu-icon"></i>
        <span class="menu-title">Categories</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="page-layout1">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{route('category.create')}}">Create New Category</a></li>

          <li class="nav-item"> <a class="nav-link" href="{{route('category.index')}}">Show All Category</a></li>


        </ul>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#page-layout2" aria-expanded="false" aria-controls="page-layouts">
        <i class="icon-box menu-icon"></i>
        <span class="menu-title">Posts</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="page-layout2">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{route('post.create')}}">Create New Post</a></li>

          <li class="nav-item"> <a class="nav-link" href="{{route('post.index')}}">Show All Posts</a></li>


        </ul>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#page-layout3" aria-expanded="false" aria-controls="page-layouts">
        <i class="icon-box menu-icon"></i>
        <span class="menu-title">Events</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="page-layout3">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{Route('event.create')}}">Create New Event</a></li>

          <li class="nav-item"> <a class="nav-link" href="{{Route('event.index')}}">Show All Events</a></li>


        </ul>
      </div>
    </li>


    <li class="nav-item">
      <a class="nav-link" href="{{route('contact.index')}}" aria-expanded="false" aria-controls="page-layouts">
        <i class="icon-box menu-icon"></i>
        <span class="menu-title">Contact Us</span>

      </a>
    </li>














  </ul>
</nav>
