-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2018 at 10:54 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yelp`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `icon`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Resturent', '<span style=\"color: rgb(34, 34, 34); font-family: Consolas, &quot;Lucida Console&quot;, &quot;Courier New&quot;, monospace; font-size: 12px; white-space: pre-wrap;\">Resturent</span><br>', 'fa fa-glass', 'cat_image_WbDk.jpg', '2018-06-09 18:54:30', '2018-06-09 18:54:30'),
(2, 'Hospitals', 'hoooooooooooo', 'fa fa-home', 'cat_image_2YCL.jpg', '2018-06-09 18:55:38', '2018-06-09 18:55:38'),
(3, 'Vehicles', '<span style=\"color: rgb(34, 34, 34); font-family: Consolas, &quot;Lucida Console&quot;, &quot;Courier New&quot;, monospace; font-size: 12px; white-space: pre-wrap;\">Vehicles</span><br>', 'fa fa-car', 'cat_image_NJqp.jpg', '2018-06-09 18:57:15', '2018-06-09 18:57:15'),
(4, 'Shoping', '<span style=\"color: rgb(34, 34, 34); font-family: Consolas, &quot;Lucida Console&quot;, &quot;Courier New&quot;, monospace; font-size: 12px; white-space: pre-wrap;\">Shoping</span><br>', 'fa fa-glass', 'cat_image_IUMr.jpg', '2018-06-09 18:58:14', '2018-06-09 18:58:14'),
(5, 'Beauty & Spa', '<span style=\"color: rgb(34, 34, 34); font-family: Consolas, &quot;Lucida Console&quot;, &quot;Courier New&quot;, monospace; font-size: 12px; white-space: pre-wrap;\">Beauty &amp; Spa</span><br>', 'fa fa-female', 'cat_image_iTjS.jpg', '2018-06-09 18:59:02', '2018-06-09 18:59:02');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `phone`, `message`, `created_at`, `updated_at`) VALUES
(1, 'yasminaa', 'yasmine.hikal@gmnnail.comu', '1222222222', 'aaaaaaaaaaaaaaaaaaa', '2018-06-17 17:54:26', '2018-06-17 17:54:26'),
(2, 'yasminaa', 'admin@admin.com', '1222222222', 'yyyyyyyyyyyyyyyy', '2018-06-17 18:12:21', '2018-06-17 18:12:21'),
(3, 'yasminaa', 'admin@admin.com', '1222222222', 'yyyyyyyyyyyyyyyy', '2018-06-17 18:12:47', '2018-06-17 18:12:47'),
(4, 'yasminaa', 'admin@admin.com', '1222222222', 'hhhhhhhhhhhhhh', '2018-06-17 19:00:28', '2018-06-17 19:00:28');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `description`, `latitude`, `longitude`, `image`, `from`, `to`, `created_at`, `updated_at`) VALUES
(1, 'Event 1', '<p>Event 1<br></p>', '27.276551611306182', '30.703368206249934', '5gaaZx.jpg', '06/08/2018', '06/13/2018', '2018-06-09 23:48:20', '2018-06-09 23:48:20'),
(2, 'Event 2', '<p>Event 1Event 1Event 1Event 1<br></p>', '27.276551611306182', '30.703368206249934', 'OkpG5v.jpg', '06/13/2018', '06/18/2018', '2018-06-09 23:48:50', '2018-06-09 23:48:50'),
(3, 'Event 3', '<p>Event 1Event 1<br></p>', '27.276551611306182', '30.703368206249934', 'm0kYah.jpg', '06/12/2018', '06/22/2018', '2018-06-09 23:49:18', '2018-06-09 23:49:18'),
(4, 'Event 4', '<p>Event 1Event 1Event 1<br></p>', '27.276551611306182', '30.703368206249934', '4y7h2W.jpg', '06/12/2018', '07/06/2018', '2018-06-09 23:49:49', '2018-06-09 23:49:49'),
(5, 'Event 5', '<p>Event 1Event 1Event 1<br></p>', '27.276551611306182', '30.703368206249934', 'eizbBK.jpg', '06/11/2018', '07/04/2018', '2018-06-09 23:50:14', '2018-06-09 23:50:14');

-- --------------------------------------------------------

--
-- Table structure for table `event_users`
--

CREATE TABLE `event_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `interested` tinyint(4) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_05_25_143505_create_categories_table', 1),
(4, '2018_05_25_143526_create_posts_table', 1),
(5, '2018_05_25_143546_create_post_media_table', 1),
(6, '2018_05_25_143651_create_rates_table', 1),
(7, '2018_05_25_143717_create_events_table', 1),
(8, '2018_05_25_143736_create_event_users_table', 1),
(9, '2018_06_01_133419_add_icon_and_image_column_to_categories_tbl', 1),
(10, '2018_06_02_145546_add_column_status_to_posts_tbl', 1),
(11, '2018_06_11_143628_add_is_restaurant_column_to_posts_tbl', 2),
(12, '2018_06_11_184706_add_colum', 3),
(13, '2018_06_15_072054_create_contacts_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_range` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opening_from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opening_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `takes_reservations` tinyint(4) NOT NULL DEFAULT '0',
  `delivery` tinyint(4) NOT NULL DEFAULT '0',
  `take_out` tinyint(4) DEFAULT '0',
  `acepet_credit` tinyint(4) NOT NULL DEFAULT '0',
  `good_for_kids` tinyint(4) NOT NULL DEFAULT '0',
  `wifi` tinyint(4) NOT NULL DEFAULT '0',
  `category_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_restaurant` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `name`, `description`, `latitude`, `longitude`, `phone`, `website`, `price_range`, `opening_from`, `opening_to`, `main_image`, `takes_reservations`, `delivery`, `take_out`, `acepet_credit`, `good_for_kids`, `wifi`, `category_id`, `status`, `created_at`, `updated_at`, `is_restaurant`) VALUES
(2, 'Museum of Art Update', '<span style=\"color: rgb(34, 34, 34); font-family: Consolas, \" lucida=\"\" console\",=\"\" \"courier=\"\" new\",=\"\" monospace;=\"\" font-size:=\"\" 12px;=\"\" white-space:=\"\" pre-wrap;\"=\"\">Welcome Description</span><br>', '27.276551611306182', '30.703368206249934', '333333333333333', 'www.facebook.comrrrr', '23-33', '4 AM', '7 PM', 'post_image_gbjr.png', 1, 1, 0, 0, 0, 0, 1, 0, '2018-06-09 20:28:01', '2018-06-11 17:23:57', 1),
(3, 'Four Seasons spa', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut, autem, quas! Aspernatur exercitationem molestias reiciendis. A corporis dolore eligendi error eveniet facilis iste, nam quas sed! At inventore quae vel!', '27.276551611306182', '30.703368206249934', '2222222222222', 'www.facebook.com', '23-33', '10 AM', '12 PM', 'post_image_H2qi.jpg', 0, 0, 0, 0, 0, 0, 1, 1, '2018-06-09 20:29:43', '2018-06-09 20:29:43', 0),
(4, 'new car', 'ytaaaaaaaaaaaaaaaaa', '27.276551611306182', '30.703368206249934', '333333', 'www.facebook.com', '23-33', '10 AM', '12 PM', 'post_image_hs7T.jpg', 0, 0, 0, 0, 0, 0, 3, 1, '2018-06-09 20:40:28', '2018-06-09 20:40:28', 0),
(5, 'Another post', 'this is description&nbsp;', '27.276551611306182', '30.703368206249934', '1222222222', 'www.facebook.com', '23-33', '10 AM', '12 PM', 'post_image_EiOs.png', 1, 1, 0, 1, 0, 1, 1, 0, '2018-06-11 12:54:44', '2018-06-11 12:54:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `post_media`
--

CREATE TABLE `post_media` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_media`
--

INSERT INTO `post_media` (`id`, `name`, `image`, `post_id`, `created_at`, `updated_at`) VALUES
(3, 'post_media_K3LW.jpg', 'post_media_K3LW.jpg', 2, '2018-06-09 20:28:01', '2018-06-09 20:28:01'),
(5, 'post_media_gson.jpg', 'post_media_gson.jpg', 2, '2018-06-09 20:28:01', '2018-06-09 20:28:01'),
(11, 'post_media_eOFe.jpg', 'post_media_eOFe.jpg', 4, '2018-06-09 20:40:28', '2018-06-09 20:40:28'),
(12, 'post_media_Uc8x.jpg', 'post_media_Uc8x.jpg', 4, '2018-06-09 20:40:28', '2018-06-09 20:40:28'),
(13, 'post_media_YM7T.jpg', 'post_media_YM7T.jpg', 4, '2018-06-09 20:40:28', '2018-06-09 20:40:28'),
(14, 'post_media_L61X.jpg', 'post_media_L61X.jpg', 4, '2018-06-09 20:40:28', '2018-06-09 20:40:28'),
(15, 'post_media_7XAi.png', 'post_media_7XAi.png', 5, '2018-06-11 12:54:44', '2018-06-11 12:54:44');

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

CREATE TABLE `rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `star_count` int(11) DEFAULT NULL,
  `review` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rates`
--

INSERT INTO `rates` (`id`, `star_count`, `review`, `post_id`, `user_id`, `created_at`, `updated_at`) VALUES
(3, 4, 'nice', 2, 4, '2018-06-09 07:00:00', NULL),
(4, 4, 'nice', 4, 1, '2018-06-09 07:00:00', NULL),
(5, 4, 'nice', 2, 2, '2018-06-09 07:00:00', NULL),
(6, 4, 'nice', 3, 6, '2018-06-09 07:00:00', NULL),
(7, 4, 'nice', 2, 7, '2018-06-09 07:00:00', NULL),
(8, 2, 'verrrrrrrrrrrrrrrrrrrrrrrry goooooooooooood', 2, 11, '2018-06-16 19:33:43', '2018-06-16 19:33:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('user','admin') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `image`, `latitude`, `longitude`, `type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'yasminaa', 'admin@admin.com', '$2y$10$jGrXCCwOVoI.C19Z.lJT7uqfMTWNgHdArFhW5nmrhbk0/yilivk9C', '45.jpg', '', '', 'admin', 'mfcZKXBbAA7ptOXcGyF4whFoD0PyGRWJ6NhilvAqtysNqnmzmVkqrDqR0rkC', '2018-06-09 18:42:54', '2018-06-09 18:42:54'),
(2, 'yassso', 'yasmine.hikal@gmail.com', '$2y$10$yb6x404SWO75fP7Hr.zN.eoVCQuD0Foas3BgeTA23L9lxzWaxQ/By', '45.jpg', '', '', 'admin', 'dB1rtQKATGqrYXBm5475iGVEdhIfgpfIo2NUdK1VXIRtV5oISDIZyxH9YKF8', '2018-06-09 18:44:30', '2018-06-09 18:44:30'),
(3, 'yasso', 'user@mail.com', '$2y$10$jGrXCCwOVoI.C19Z.lJT7uqfMTWNgHdArFhW5nmrhbk0/yilivk9C', '1.jpg', '', '', 'user', '4aJJo1qjPXXw9VHjST6iY60nUwkC0u18jRUzZDInoJdUi0jUXTJKgWxakyNQ', '2018-06-09 18:42:54', '2018-06-09 18:42:54'),
(4, 'yasso', 'userw@mail.com', '$2y$10$jGrXCCwOVoI.C19Z.lJT7uqfMTWNgHdArFhW5nmrhbk0/yilivk9C', '4.jpg', '', '', 'user', '4aJJo1qjPXXw9VHjST6iY60nUwkC0u18jRUzZDInoJdUi0jUXTJKgWxakyNQ', '2018-06-09 18:42:54', '2018-06-09 18:42:54'),
(5, 'yassso', 'yasmine@gmail.com', '$2y$10$yb6x404SWO75fP7Hr.zN.eoVCQuD0Foas3BgeTA23L9lxzWaxQ/By', '2.jpg', '', '', 'user', NULL, '2018-06-09 18:44:30', '2018-06-09 18:44:30'),
(6, 'yassso', 'yas3ine@gmail.com', '$2y$10$yb6x404SWO75fP7Hr.zN.eoVCQuD0Foas3BgeTA23L9lxzWaxQ/By', '5.jpg', '', '', 'user', NULL, '2018-06-09 18:44:30', '2018-06-09 18:44:30'),
(7, 'yass3so', 'yasmi3ne@gmail.com', '$2y$10$yb6x404SWO75fP7Hr.zN.eoVCQuD0Foas3BgeTA23L9lxzWaxQ/By', '3.jpg', '', '', 'user', NULL, '2018-06-09 18:44:30', '2018-06-09 18:44:30'),
(8, 'yasminaa', 'y44asmine.hikal@gmail.com', '$2y$10$ZkTW0Ts8twq.0AN60QTijOgFbik5g9H4h.TW0U6Js2itAo4ehETNa', NULL, '', '', 'user', 'GcdAxplKiAoFmKxKzkenfhXNrVnZrhN2K1LpOCxODnJDsqB9uWSuabiNt9Qd', '2018-06-10 05:31:49', '2018-06-10 05:31:49'),
(9, 'yasminaa', 'yas8so@gmail.com', '$2y$10$8BqCGVg8s/l66FC.DtZZyed.WiPh1XUxgGYkYpyykMcNYjupJKtYW', NULL, '', '', 'user', 'xsvxxXv1YOD3D0P27HBu27J0iiOUubCZTOOvjPsI73RT9XfhuzokEO4FOtJt', '2018-06-09 21:52:50', '2018-06-09 21:52:50'),
(10, 'yasminaaa', 'yasso@gmail.com', '$2y$10$jB5ZRclkM8XwQ7UjTTU5.uxvhNAB.0WR71aOcAbLAwBLm69lQJ4sq', NULL, '', '', 'user', NULL, '2018-06-09 21:56:14', '2018-06-09 21:56:14'),
(11, 'yasminaa', 'yasmine.hikal@gmnnail.comu', '$2y$10$HlBHsPWxFNoXiQPh8z2rEetgjM/XJpP1dMjVeR7TfGmkyiykUnyma', 'user_image_zbG4.jpg', '29.633108999855065', '30.571532268749934', 'user', 'CfdGoSVVxHs2mMOXJEjGuf5GkZNbkv9FMnKCSiYQwA4uCAnqc5k1s9qwQiBD', '2018-06-13 11:37:27', '2018-06-16 18:07:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_users`
--
ALTER TABLE `event_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_users_user_id_foreign` (`user_id`),
  ADD KEY `event_users_event_id_foreign` (`event_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_category_id_foreign` (`category_id`);

--
-- Indexes for table `post_media`
--
ALTER TABLE `post_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_media_post_id_foreign` (`post_id`);

--
-- Indexes for table `rates`
--
ALTER TABLE `rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rates_post_id_foreign` (`post_id`),
  ADD KEY `rates_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `event_users`
--
ALTER TABLE `event_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `post_media`
--
ALTER TABLE `post_media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `rates`
--
ALTER TABLE `rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `event_users`
--
ALTER TABLE `event_users`
  ADD CONSTRAINT `event_users_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `event_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_media`
--
ALTER TABLE `post_media`
  ADD CONSTRAINT `post_media_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `rates`
--
ALTER TABLE `rates`
  ADD CONSTRAINT `rates_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rates_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
