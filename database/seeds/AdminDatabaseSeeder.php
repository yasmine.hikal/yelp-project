<?php

use Illuminate\Database\Seeder;
use App\User; 

class AdminDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User ; 
        $admin->email = 'admin@gmail.com' ;
        $admin->password = crypt(123456,'') ; 
        $admin->image = 'test.jpg' ; 
        $admin->type = 'admin' ; 
        $admin->save();   
       

    }
}
