<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('phone')->nullable();
            $table->string('website')->nullable();
            $table->string('price_range')->nullable();
            $table->string('opening_from')->nullable();
            $table->string('opening_to')->nullable();
            $table->string('main_image')->nullable();
            $table->tinyInteger('takes_reservations');
            $table->tinyInteger('delivery');
            $table->tinyInteger('take_out');
            $table->tinyInteger('acepet_credit');
            $table->tinyInteger('good_for_kids');
            $table->tinyInteger('wifi');

            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post');
    }
}
