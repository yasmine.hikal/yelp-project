<?php

namespace App\Http\Controllers\Frontend;

use App\Category;
use App\Event;
use App\Post;
use App\Rate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    //

    public function index(){

    $categories=Category::with('postcount')
        ->orderBy('created_at','desc')
        ->get();

    $posts=Post::with('post_media')
        ->with('category')
        ->with('post_rates')
        ->with('post_rates_count')
        ->orderBy('created_at','desc')
        ->get();


    $events=Event::latest()->take(4)->get();


    $rates=Rate::with('post')
        ->with('user')
        ->orderBy('created_at','desc')
        ->get();

    $users=User::where('type','user')
        ->orderBy('created_at','desc')
        ->get();

        return view('frontend.pages.index',compact('events','users','rates','categories','posts'));
    }




}
