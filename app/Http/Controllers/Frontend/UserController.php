<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth ; 
class UserController extends Controller
{
    public function index(){

        if (Auth::user()->type == 'admin'){
            return redirect()->intended('dashboard'); 
        } else {


            $user=Auth::user();
            return view('frontend.pages.profile',compact('user')) ;
        }

    }


    public function update_profile(Request $request){

        $rules=[
          'name'=>'required',
          'email'=>'required',

        ];

        $this->validate($request,$rules);


        $user=Auth::user();
        $user->name=$request->name;
        $user->email=$request->email;

        if($request->hasFile('image')){

            $image = $request->file('image');
            $extension = $image->getClientOriginalExtension();
            $image_rename = 'user_image_'.str_random(4). '.' .$extension;
            $image->move(public_path('uploads/users_images/'), $image_rename) ;
            $user->image = $image_rename ;
        }
        $user->latitude=$request->latitude;
        $user->longitude=$request->longitude;
        $user->save();

        session()->flash('success' ,'User Updated Successfully');

        return redirect()->back();

    }



}
