<?php

namespace App\Http\Controllers\Frontend;

use App\Category;
use App\Post;
use App\Rate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB ;

class PostController extends Controller
{
    //

    public function index(Request $request ,$id){

        $posts=Post::where('category_id',$id)
            ->with('post_media')
            ->with('post_rates')
            ->with('post_rates_count')
            ->with('category')
            ->orderBy('created_at','desc')
            ->get();

        $category=Category::find($id)->name;

        $categories=Category::with('postcount')
            ->orderBy('created_at','desc')
            ->get();

        $recent_posts=Post::with('post_media')
            ->with('category')
            ->with('post_rates')
            ->with('post_rates_count')
            ->orderBy('created_at','desc')
            ->take('4')
            ->get();

        return view('frontend.pages.posts',compact('recent_posts','categories','posts','category'));

    }

   public function post_details(Request $request ,$id){

     $post =Post::where('id',$id)
           ->with('post_media')
           ->with('post_rates')
           ->with('post_rates_count')
           ->with('category')
           ->orderBy('created_at','desc')
           ->first();




           $category=Category::find($post->category->id)->name;






       return view('frontend.pages.post_details',compact('post','category'));

    }


    public function review(Request $request , $id)
    {

        $review=new Rate();
        $review->user_id=Auth::user()->id;
        $review->post_id=$id;
        $review->review=$request->review;
        $review->star_count=$request->rate;
        $review->save();

        return back();

    }


    public function search(Request $request ){

        $posts=Post::where('category_id',$request->category_id)
            ->where('name','like','%'.$request->key.'%')
            ->with('post_media')
            ->with('post_rates')
            ->with('post_rates_count')
            ->with('category')
            ->orderBy('created_at','desc')
            ->get();

        $category=Category::find($request->category_id)->name;


        $categories=Category::with('postcount')
            ->orderBy('created_at','desc')
            ->get();

        $recent_posts=Post::with('post_media')
            ->with('category')
            ->with('post_rates')
            ->with('post_rates_count')
            ->orderBy('created_at','desc')
            ->take('4')
            ->get();

        return view('frontend.pages.posts',compact('posts','recent_posts','category','categories'));

    }


    public function get_location_search(){


        $categories = Category::all();


        return view('frontend.pages.location_search' , compact('categories'));

    }

    public function post_location_search(Request $request){

        $lat = $request->lat ;
        $long = $request->long ;

        $posts = DB::table('posts')
            ->selectRaw("
            * , (
              6371 * acos (
              cos ( radians($lat) )
              * cos( radians( latitude ) )
              * cos( radians( longitude ) - radians($long) )
              + sin ( radians($lat) )
              * sin( radians( latitude ) )
            )
            ) AS distance")
            ->where('category_id', '=', $request->category_id)
            ->having('distance' , '<=' , 80)
            ->get();






           }


}
