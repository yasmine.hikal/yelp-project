<?php

namespace App\Http\Controllers\Frontend;

use App\Event;
use App\EventUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    //
    public function index(){

        $events=Event::all();
        return view('frontend.pages.events',compact('events'));

    }

    public function event_details($id){

        $event=Event::find($id);


            $event_user=EventUser::where([
                'event_id'=>$id,
                'user_id'=>Auth::user()->id,
            ])->first();



        return view('frontend.pages.event_details',compact('event','event_user'));

    }

    public function interest_event(Request $request){


        $event_user_old=EventUser::where([
            'event_id'=>$request->event_id,
            'user_id'=>Auth::user()->id,
        ])->first();

        if($event_user_old){

            $event_user_old->interested=$request->option;
            $event_user_old->save();
        }else{
            $event_user= new EventUser();
            $event_user->user_id=Auth::user()->id;
            $event_user->event_id=$request->event_id;
            $event_user->interested=$request->option;
            $event_user->save();

        }

    }

}
