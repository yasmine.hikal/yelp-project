<?php

namespace App\Http\Controllers\Backend;

use App\contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactUsController extends Controller
{
    //

    public function index()
    {
        $contacts = contact::all();
        return view('backend.modules.contact_us.index' , compact('contacts'));
    }

    public function destroy($id)
    {
        $contact=contact::find($id);
        $contact->delete();

        session()->flash('success' , trans('Contact Deleted Successfully'));
        return redirect()->route('contact_us.index');
    }

}
