<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Category ; 
use Validator ; 
use Session ; 
use File ; 

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $categories = Category::all();  
       return view('backend.modules.category.index' , compact('categories')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.modules.category.create') ; 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $rules = [

            'cat_name' => 'required' , 
            'cat_description' => 'required' , 
            'cat_icon'  => 'required' 
        ];
        
        
       $this->validate($request , $rules) ; 

       $category                = new Category ; 
       $category->name          = $request->cat_name ; 
        // simple hack to strip 
        $tags = array("<p>", "</p>", "<font>", "</font>");
        $string = $request->cat_description ; 
        $p_cleaned =  str_replace($tags, "", $string);

        $category->description   = $p_cleaned ; 
       $category->icon          = $request->cat_icon ; 

       if($request->hasFile('cat_image')){

            $image = $request->file('cat_image'); 
			$extension = $image->getClientOriginalExtension();
            $image_rename = 'cat_image_'.str_random(4). '.' .$extension;
            $image->move(public_path('uploads/categories/'), $image_rename) ;
            $category->image = $image_rename ; 

       }

      $category->save(); 
      
      Session::flash('cat_added' , 'Category has been added successfully') ; 

      return redirect()->route('category.index') ; 



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $category = Category::find($id) ; 
         return view('backend.modules.category.edit' , compact('category')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $rules = [

            'cat_name' => 'required' , 
            'cat_description' => 'required' , 
            'cat_icon'  => 'required' 
        ];
            
            
        $this->validate($request , $rules) ; 


         

        $category                = Category::find($id) ; 
        $category->name          = $request->cat_name ; 

        // simple hack to strip 
        $tags = array("<p>", "</p>", "<font>", "</font>");
        $string = $request->cat_description ; 
        $p_cleaned =  str_replace($tags, "", $string);

        $category->description   = $p_cleaned ; 
        $category->icon          = $request->cat_icon ; 

        if($request->hasFile('cat_image')){


                // find old image then delete it  -_- 
                $old_image  = $category->image ; 
            	$old_image_path   = public_path()."/uploads/categories/".$old_image;
            	File::delete($old_image_path);

                $image = $request->file('cat_image'); 
                $extension = $image->getClientOriginalExtension();
                $image_rename = 'cat_image_'.str_random(4). '.' .$extension;
                $image->move(public_path('uploads/categories/'), $image_rename) ;
                $category->image = $image_rename ; 

        }

        $category->save(); 
        
        Session::flash('cat_updated' , 'Category has been updated successfully') ; 

        return redirect()->route('category.index') ; 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {

         
    // }
}
