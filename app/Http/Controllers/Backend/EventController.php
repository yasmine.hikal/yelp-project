<?php

namespace App\Http\Controllers\Backend;

use App\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $events=Event::get();
        return view('backend.modules.event.index',compact('events'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.modules.event.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      
        $rules=[
          'name'=>'required',
          'description'=>'required',
          'image'=>'required',
           'from'=>'required',
           'to'=>'required',
        ];

        $this->validate($request,$rules);
        $event=new Event();
        $event->name=$request->name;
        $event->description=$request->description;
        $event->from=$request->from;
        $event->to=$request->to;
        $event->latitude= $request->latitude;
        $event->longitude= $request->longitude;
        if($request->image){
            $image= $request->file('image');
            $image_rename = str_random(6). '.' .$image->getClientOriginalExtension();
            $image->move(public_path('uploads/events_images/'), $image_rename) ;
            $event->image = $image_rename ;
        }

        $event->save();


        session()->flash('success' , trans('New Event Added Successfully'));
        return redirect()->route('event.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event=Event::find($id);
        return view('backend.modules.event.edit',compact('event'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $rules=[
            'name'=>'required',
            'description'=>'required',
            
            'from'=>'required',
            'to'=>'required',
        ];

        $this->validate($request,$rules);
        $event=Event::find($id);
        $event->name=$request->name;
        $event->description=$request->description;
        $event->from=$request->from;
        $event->to=$request->to;
        $event->latitude=$request->latitude;
        $event->longitude=$request->longitude;
        if($request->image){
            $image= $request->file('image');
            $image_rename = str_random(6). '.' .$image->getClientOriginalExtension();
            $image->move(public_path('uploads/events_images/'), $image_rename) ;
            $event->image = $image_rename ;
        }

        $event->save();


        session()->flash('success' , trans('New Event Added Successfully'));
        return redirect()->route('event.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event=Event::find($id);
        $event->delete();

        session()->flash('success' , trans('Event Deleted Successfully'));
        return redirect()->route('event.index');
    }
}
