<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use App\Post;
use App\PostMedia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Session ; 
use File ; 
use Auth ; 

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts=Post::all();
        return view('backend.modules.post.index',compact('posts'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Categories=Category::get();

        return view('backend.modules.post.create',compact('Categories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->all());
        
        $rules = [

            'name' => 'required' , 
            'description' => 'required' , 
            'main_image' => 'required' , 
             
        ];  

        $messages = [

            'name.required'         => 'please enter the name ' , 
            'description.required'  => 'please enter post description ' , 
            'name.required'         => 'Please make sure to select main image for the post ' , 
        ];

        $this->validate($request , $rules , $messages) ; 

        $post = new Post ; 
        $post->name = $request->name ; 


        $tags = array("<p>", "</p>", "<font>", "</font>");
        $string = $request->description ; 
        $p_cleaned =  str_replace($tags, "", $string);

        $post->description = $p_cleaned ; 

        if($request->hasFile('main_image')){

            $image = $request->file('main_image'); 
			$extension = $image->getClientOriginalExtension();
            $image_rename = 'post_image_'.str_random(4). '.' .$extension;
            $image->move(public_path('uploads/posts/'), $image_rename) ;
            $post->main_image = $image_rename ; 
        }


        $post->phone = $request->phone ; 
        $post->website = $request->website ; 
        $post->price_range = $request->price_range ; 
        $post->opening_from = $request->opening_from ; 
        $post->opening_to = $request->opening_to ; 

        $post->takes_reservations = $request->takes_reservations == 'on' ? '1' : '0' ; 
        $post->delivery = $request->delivery == 'on' ? '1' : '0' ; 
        $post->take_out = $request->take_out == 'on' ? '1' : '0' ; 
        $post->acepet_credit = $request->acepet_credit == 'on' ? '1' : '0' ; 
        $post->good_for_kids = $request->good_for_kids == 'on' ? '1' : '0' ; 
        $post->wifi = $request->wifi == 'on' ? '1' : '0' ; 

        $post->category_id = $request->category_id ; 
        $post->latitude = $request->latitude ; 
        $post->longitude = $request->longitude ; 
        $post->status = $request->status ;

        $post->is_restaurant = $request->is_restaurant == 'on' ? '1':'0' ;

        $post->save(); 

        
        if($request->hasFile('post_media')){
             $attachements = $request->file('post_media'); 
            foreach($attachements as $post_media){
              
                $extension = $post_media->getClientOriginalExtension();
                
                $image_rename = 'post_media_'.str_random(4). '.' .$extension;
                $post_media->move(public_path('uploads/posts/post_media'), $image_rename) ;
                
                
                $post_media = new PostMedia ;
                $post_media->name = $image_rename ; 
                $post_media->image = $image_rename ; 
                $post_media->post_id = $post->id ; 

                $post_media->save(); 

            }

          
        }


        Session::flash('post_added' , 'Post has been added successfully') ; 

        return redirect()->route('post.index') ; 


       
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id) ;
        $Categories =Category::get();
        return view('backend.modules.post.edit' , compact('post', 'Categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [

            'name' => 'required' ,
            'description' => 'required' ,


        ];

        $messages = [

            'name.required'         => 'please enter the name ' ,
            'description.required'  => 'please enter post description ' ,

        ];

        $this->validate($request , $rules , $messages) ;

        $post = Post::find($id) ;
        $post->name = $request->name ;


        $tags = array("<p>", "</p>", "<font>", "</font>");
        $string = $request->description ;
        $p_cleaned =  str_replace($tags, "", $string);

        $post->description = $p_cleaned ;

        if($request->hasFile('main_image')){


            $old_image  = $post->main_image ;
            $old_image_path   = public_path()."/uploads/posts/".$old_image;
            File::delete($old_image_path);

            $image = $request->file('main_image');
            $extension = $image->getClientOriginalExtension();
            $image_rename = 'post_image_'.str_random(4). '.' .$extension;
            $image->move(public_path('uploads/posts/'), $image_rename) ;
            $post->main_image = $image_rename ;
        }


        $post->phone = $request->phone ;
        $post->website = $request->website ;
        $post->price_range = $request->price_range ;
        $post->opening_from = $request->opening_from ;
        $post->opening_to = $request->opening_to ;

        $post->takes_reservations = $request->takes_reservations == 'on' ? '1' : '0' ;
        $post->delivery = $request->delivery == 'on' ? '1' : '0' ;
        $post->take_out = $request->take_out == 'on' ? '1' : '0' ;
        $post->acepet_credit = $request->acepet_credit == 'on' ? '1' : '0' ;
        $post->good_for_kids = $request->good_for_kids == 'on' ? '1' : '0' ;
        $post->wifi = $request->wifi == 'on' ? '1' : '0' ;

        $post->category_id = $request->category_id ;
        $post->latitude = $request->latitude ;
        $post->longitude = $request->longitude ;
        $post->status = $request->status ;

        $post->is_restaurant = $request->is_restaurant == 'on' ? '1':'0' ;

        $post->save();


        if($request->hasFile('post_media')){
            $attachements = $request->file('post_media');
            foreach($attachements as $post_media){

                $extension = $post_media->getClientOriginalExtension();

                $image_rename = 'post_media_'.str_random(4). '.' .$extension;
                $post_media->move(public_path('uploads/posts/post_media'), $image_rename) ;


                $post_media = new PostMedia ;
                $post_media->name = $image_rename ;
                $post_media->image = $image_rename ;
                $post_media->post_id = $post->id ;

                $post_media->save();

            }


        }


        Session::flash('post_updated' , 'Post has been updated successfully') ;

        return redirect()->route('post.index') ;


    }

    public function deleteImage(Request $request){

        $id =  $request['image_id'];

        $find_post_media = PostMedia::find($id) ;

        // find old image then delete it  -_-
        $old_image  = $find_post_media->image ;
        $old_image_path   = public_path()."/uploads/posts/post_media/".$old_image;
        File::delete($old_image_path);


        // delete the record it self  :
        $find_post_media->destroy($id) ;

        return response()->json([

            'status'=> 'success' ,
            'msg'   => 'Attachment has been deleted successfully'
        ]);



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
