<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function category(){
        return $this->belongsTo('App\Category' , 'category_id'); 
    }

    public function post_media(){
        return $this->hasMany('App\PostMedia');
    }

    public function post_rates(){
        return $this->hasMany('App\Rate');
    }

     public function post_rates_count(){
        return $this->post_rates()
            ->selectRaw('post_id , count(post_id) as post_rates_count ')
            ->groupBy('post_id');
    }




}
