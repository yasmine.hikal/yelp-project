<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    public function post(){
        return $this->hasMany('App\Post');
    }

    public function postcount(){
        return $this->post()
            ->selectRaw('category_id, count(category_id) as postcount')
            ->groupBy('category_id');
    }



}
